var width = document.documentElement.clientWidth;
var height = document.documentElement.clientHeight;

var center = {
    'x': width / 2,
    'y': height / 2
};

var firstPoint;
var firstVector;

var previousAngle = 360;
var totalAngle = 0;
var currentAngle = 0;

var isMoving = false;

// повертається на кут між векторами А (центр -> точка кліку мишкою) і В (центр -> поточна точка)
function rotate(x, y) {
    let currentVector = { x: x - center.x, y: y - center.y };

    let numerator = currentVector.x * firstVector.x + currentVector.y * firstVector.y;
    let denominator = Math.sqrt(currentVector.x * currentVector.x + currentVector.y * currentVector.y)
        * Math.sqrt(firstVector.x * firstVector.x + firstVector.y * firstVector.y);

    let angle = Math.round(Math.acos(numerator / denominator) * 180 / Math.PI);
    console.log(angle);

    let currentSide = ((firstPoint.x - center.x) * (y - center.y) - (firstPoint.y - center.y) * (x - center.x)) > 0 ? 'r' : 'l';
    if (currentSide === 'l') {
        angle = -angle;
    }

    if (previousAngle === angle || (totalAngle + angle) > 340) {
        return;
    }

    currentAngle = angle;

    let resultAngle = totalAngle + angle;
    let opacity = resultAngle < 0 ? (360 + resultAngle) : resultAngle;
    opacity /= 340;

    document.getElementsByClassName('fader')[0].style.transform = 'rotate(' + resultAngle + 'deg)';
    document.getElementById('square').style.opacity = opacity;
}

// повертається на кут між векторами А (центр -> початкова точка відліку (50%, 0)) і В (центр -> поточна точка)
/*function rotate(x, y) {
    let currentVector = { x: x - center.x, y: y - center.y };
    let mainPoint = { x: center.x, y: -center.y };
    let mainVector = { x: mainPoint.x - center.x, y: mainPoint.y - center.y };

    let numerator = currentVector.x * mainVector.x + currentVector.y * mainVector.y;
    let denominator = Math.sqrt(currentVector.x * currentVector.x + currentVector.y * currentVector.y)
        * Math.sqrt(mainVector.x * mainVector.x + mainVector.y * mainVector.y);

    let angle = Math.round(Math.acos(numerator / denominator) * 180 / Math.PI);

    let currentSide = ((mainPoint.x - center.x) * (y - center.y) - (mainPoint.y - center.y) * (x - center.x)) > 0 ? 'r' : 'l';

    if (currentSide === 'l') {
        angle = 360-angle;
    }

    if (previousAngle === angle || angle > 340) {
        return;
    }

    document.getElementsByClassName('fader')[0].style.transform = 'rotate(' + angle + 'deg)';
}*/

function handlerStart(event) {
    let coordinates;

    if (event.type === 'touchstart') {
        coordinates = { x: event.touches[0].clientX, y: event.touches[0].clientY };
    } else {
        coordinates = { x: event.clientX, y: event.clientY };
    }

    isMoving = true;

    firstPoint = { x: coordinates.x, y: coordinates.y };
    firstVector = { x: firstPoint.x - center.x, y: firstPoint.y - center.y };
}

function handlerEnd() {
    isMoving = false;
    totalAngle += (totalAngle >= 340 && currentAngle > 0) ? 0 : currentAngle;
}

function handlerMove(event) {
    if (isMoving) {
        let coordinates;

        if (event.type === 'touchmove') {
            coordinates = { x: event.touches[0].clientX, y: event.touches[0].clientY };
        } else {
            coordinates = { x: event.clientX, y: event.clientY };
        }

        rotate(coordinates.x, coordinates.y);
    }
}

['mousedown', 'touchstart'].map((item) => document.addEventListener(item, handlerStart));
['mouseup', 'touchend'].map((item) => document.addEventListener(item, handlerEnd));
['mousemove', 'touchmove'].map((item) => document.addEventListener(item, handlerMove));

window.addEventListener('resize', function () {
    width = document.documentElement.clientWidth;
    height = document.documentElement.clientHeight;

    center = {
        'x': width / 2,
        'y': height / 2
    };
});
