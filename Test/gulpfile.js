'use strict';

const gulp         = require('gulp'),                           // подключаем Gulp
    webserver      = require('browser-sync'),                   // сервер для работы и автоматического обновления страниц
    plumber        = require('gulp-plumber'),                   // модуль для отслеживания ошибок Gulp
    rigger         = require('gulp-rigger'),                    // модуль для импорта содержимого одного файла в другой
    sourcemaps     = require('gulp-sourcemaps'),                // модуль для генерации карты исходных файлов
    sass           = require('gulp-sass'),                      // модуль для компиляции SASS (SCSS) в CSS
    postcss        = require('gulp-postcss'),                   // postCss
    autoprefixer   = require('autoprefixer'),                   // модуль для автоматической установки автопрефиксов
    rename         = require('gulp-rename'),
    cssnano        = require('cssnano'),
    postfocus      = require('postcss-focus'),
    lost           = require('lost'),
    mqpacker       = require("css-mqpacker"),
    concat         = require('gulp-concat'),
    uglify         = require('gulp-uglify-es').default,
    consolidate    = require('gulp-consolidate'),
    iconfont       = require('gulp-iconfont'),
    gulpif         = require('gulp-if'),
    postcolor      = require('postcss-color-function'),
    postshort      = require('postcss-short');


var autoprefixerList = [
    'Chrome >= 45',
    'Firefox ESR',
    'Edge >= 12',
    'Explorer >= 10',
    'iOS >= 9',
    'Safari >= 9',
    'Android >= 4.4',
    'Opera >= 30'
];

gulp.task('fonts:icons', function () {
    return gulp.src('src/fonts/icons/**/*.svg')
        .pipe(plumber())
        .pipe(iconfont({
            fontName: 'iconfont',
            formats: ['woff', 'woff2'],
            appendCodepoints: true,
            appendUnicode: false,
            normalize: true,
            fontHeight: 1000,
            centerHorizontally: true
        }))
        .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('js:rigger', () => {
    return gulp.src('src/main.js')
        .pipe(plumber())
        .pipe(rigger())
        .pipe(sourcemaps.init())
       // .pipe(uglify())       // минимизация
        .pipe(sourcemaps.write('./'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('js:components', () => {
    return gulp.src('src/components/**/*.js')
            .pipe(plumber())
            .pipe(uglify())
            .pipe(gulp.dest('dist/assets/js/components'));
});


gulp.task('js', gulp.series('js:rigger', 'js:components',() => {
    return gulp.src('src/js/**/*.js')
            .pipe(plumber())
            .pipe(uglify())
            .pipe(gulp.dest('dist/assets/js'));
}));


gulp.task('css:sass', () => {
    return gulp.src('src/main.scss')
        .pipe(plumber())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'uncompressed'
        }))
        .pipe(postcss([
            lost(),
            mqpacker({sort : true}),
            postfocus(),
            postshort(),
            autoprefixer({
                overrideBrowserslist: autoprefixerList
            }),
            cssnano({
                minifyFontWeight: false,
                calc: {precision: 4}
            })
        ]))
        .pipe(sourcemaps.write('.'))

        .pipe(gulp.dest('dist/assets/css'));
});

gulp.task('browser-sync', () => {
    webserver({
        server: {
            baseDir : './'
        },
        port : 3030
    });
});

gulp.task('watch:css', () => {
    gulp.watch('src/**/*.{scss,sass}', {delay: 100}, gulp.series('css:sass', (done) => {
        webserver.reload({stream: false});
        done();
    }));
});

gulp.task('watch:js', () => {
    gulp.watch('src/**/*.js', gulp.series('js', (done) => {
        webserver.reload({stream: true});
        done();
    }));
});


gulp.task('watch', gulp.parallel('watch:css', 'watch:js'));

gulp.task('build', gulp.series('css:sass', 'js'));

gulp.task('prod', gulp.series('js', 'css:sass'));

gulp.task('default', gulp.series('build', gulp.parallel('watch', 'browser-sync')));