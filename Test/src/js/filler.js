import { fillPageWithFooter } from "./components/footer/footer.js";

function fillMainWithMenu(modules, handlerClick) {
    console.log('fillMainWithMenu');

    let main = document.getElementById('main');
    main.innerHTML = '';

    document.getElementById('footer').innerHTML = "";

    let title = document.createElement('p');
    title.classList.add('main__title');
    title.innerHTML = 'Вітаємо на терміналі самообслуговування!';

    let subtitle = document.createElement('p');
    subtitle.classList.add('main__subtitle');
    subtitle.innerHTML = 'Будь ласка, оберіть ціль вашого звернення';

    let options = document.createElement('div');
    options.classList.add('main__options');

    for (let module of modules) {
        let option = document.createElement('div');
        option.classList.add('main__option', module.progName.toLowerCase());
        option.setAttribute('id', module.id);
        option.addEventListener('click', handlerClick);

        let image = document.createElement('img');
        image.setAttribute('src', '../../images/' + module.progName.toLowerCase() + '.png');
        image.classList.add('main__option-image');

        let text = document.createElement('p');
        text.classList.add('main__option-text');
        text.innerHTML = module.menuText;

        option.appendChild(image);
        option.appendChild(text);
        options.appendChild(option);
    }

    main.appendChild(title);
    main.appendChild(subtitle);
    main.appendChild(options);
}

function fillMainWithServices(path, modules, handlerClick) {
    console.log('fillMainWithServices');

    let main = document.getElementById('main');
    main.innerHTML = '';

    let module = getModuleById(path[path.length - 1].id, modules);
    let serviceGroups = module.serviceGroups;

    let { indent, size } = getIndentAndSize(serviceGroups.length);

    let pathEl = document.createElement('p');
    pathEl.classList.add('main__path');
    pathEl.innerHTML = getPathText(path);

    let question = document.createElement('p');
    question.classList.add('main__question', indent);
    question.setAttribute('id', 'question');
    question.innerHTML = 'Будь ласка оберіть послугу';

    let answers = document.createElement('div');
    answers.classList.add('main__answers');
    answers.classList.add(indent);

    for (let serviceGroup of serviceGroups) {
        let answer = document.createElement('div');
        answer.classList.add('main__answer', size);

        let text = document.createElement('span');
        text.classList.add('main__answer-text', serviceGroup.name.length > 35 ? 'u-fontSize-s' : 'u-fontSize-m');
        text.style.fontSize = serviceGroup.name.length > 35 ? '18px' : '22px';
        text.setAttribute('id', serviceGroup.id);
        text.innerHTML = serviceGroup.name;

        answer.addEventListener('click', handlerClick);
        answer.appendChild(text);
        answers.appendChild(answer);
    }

    main.appendChild(pathEl);
    main.appendChild(question);
    main.appendChild(answers);

    fillPageWithFooter('Головна');
}

function fillMainWithQuestion(path, questionData, handlerClick, startIndex = 0) {
    console.log('fillMainWithQuestion');

    let main = document.getElementById('main');
    main.innerHTML = '';

    let answers = questionData.answers;

    let { indent, size } = getIndentAndSize(answers.length - answers.length * startIndex);

    let pathEl = document.createElement('p');
    pathEl.classList.add('main__path');
    pathEl.innerHTML = getPathText(path);

    let question = document.createElement('p');
    question.classList.add('main__question', indent);
    question.setAttribute('id', 'question');
    question.innerHTML = questionData.fullText;

    let answersTag = document.createElement('div');
    answersTag.classList.add('main__answers', indent);

    let fontParams = answers.map((answer) => {
        return answer.text.length > 35 && answers.length > 3;
    }).includes(true) ? { size: '18px', height: '24.5px' } : { size: '22px', height: '30px' };

    let linkAmount = (answers.length - answers.length % 9) / 9;
    let lastIndex = answers.length > startIndex * 9 + 9 ? startIndex * 9 + 9 : answers.length;

    for (let i = startIndex * 9; i < lastIndex; i++) {
        let answer = answers[i];
        let answerTag = document.createElement('div');
        answerTag.classList.add('main__answer', size);
        answerTag.addEventListener('click', function() {
            handlerClick(answer, questionData);
        });

        let text = document.createElement('p');
        text.classList.add('main__answer-text');
        text.style.fontSize = fontParams.size;
        text.style.lineHeight = fontParams.height;

        let additionText = document.createElement('p');
        additionText.classList.add('main__answer-addition');

        if (answer.text.includes('(')) {
            let startIndex = answer.text.indexOf('(');
            text.innerHTML = answer.text.slice(0, startIndex);
            additionText.innerHTML = answer.text.slice(startIndex);
        } else {
            text.innerHTML = answer.text;
        }

        answerTag.appendChild(text);
        answerTag.appendChild(additionText);

        answersTag.appendChild(answerTag);
    }

    main.appendChild(pathEl);
    main.appendChild(question);
    main.appendChild(answersTag);

    if (linkAmount > 0) {
        let linksContainer = document.createElement('div');
        linksContainer.classList.add('main__links');

        for (let i = 0; i <= linkAmount; i++) {
            let link = document.createElement('div');
            link.classList.add('main__link');
            link.setAttribute('id', String(i));
            link.addEventListener('click', function () {
                fillMainWithQuestion(path, questionData, handlerClick, i);
            });

            linksContainer.appendChild(link);
        }

        linksContainer.children[startIndex].classList.add('main__link--selected');
        main.appendChild(linksContainer);
    }

    fillPageWithFooter('Назад', 'Головна');
}

function fillMainWithResults(path, handlerClick) {
    console.log('fillMainWithResults');

    let main = document.getElementById('main');
    main.innerHTML = '';

    let pathEl = document.createElement('p');
    pathEl.classList.add('main__path');
    pathEl.innerHTML = getPathText(path, false) + '&nbsp&nbsp&nbsp/&nbsp&nbsp&nbsp' + 'Підтвердження даних';

    let infoContainer = document.createElement('div');
    infoContainer.classList.add('main__info');

    for (let p of path) {
        if (p.type === 'answer') {
            let row = document.createElement('div');
            row.classList.add('main__info-row');

            let question = document.createElement('p');
            question.classList.add('main__info-question');
            question.innerHTML = p.previousQuestion.briefText;

            let answer = document.createElement('p');
            answer.classList.add('main__info-answer');
            answer.innerHTML = getFormattedText(p.text);

            let edit = document.createElement('button');
            edit.classList.add('main__info-edit');
            edit.innerHTML = 'Редагувати';
            edit.addEventListener('click', function () {
                console.log('----');
                handlerClick(p);
            });

            row.appendChild(question);
            row.appendChild(answer);
            row.appendChild(edit);

            infoContainer.appendChild(row);
        }
    }

    main.appendChild(pathEl);
    main.appendChild(infoContainer);

    fillPageWithFooter('Назад', 'Головна', 'Далі');
}

function fillMainWithUserParams(userParams) {
    console.log('fillMainWithUserParams');
    console.log(userParams);
    let main = document.getElementById('main');
    main.innerHTML = '';

    // TODO edit - тут твоя клавіатура і логіка

    let message = document.createElement('p');
    message.classList.add('main__question');
    message.innerHTML = userParams.prompt1;

    main.appendChild(message);

    fillPageWithFooter('Назад', 'Головна', 'Далі');
}

function fillMainWithUndefined() {
    let main = document.getElementById('main');
    main.innerHTML = '';

    let message = document.createElement('p');
    message.classList.add('main__question');
    message.innerHTML = 'Нажаль немає інформації по даному запиту';

    main.appendChild(message);

    fillPageWithFooter('Назад', 'Головна');
}


// handlers


// helpers
function getModuleById(id, modules) {
    for (let module of modules) {
        if (module.id === id) {
            return module;
        }
    }
}

function getPathText(path, isEntire = true) {
    let text = 'Головна';

    for (let p of path) {
        if (!isEntire && p.type === 'answer') {
            continue;
        }
        text += '&nbsp&nbsp&nbsp/&nbsp&nbsp&nbsp' + getFormattedText(p.text);
    }

    return text;
}

function getIndentAndSize(length) {
    let indent = length > 6 ? ( length > 9 ? 'u-indent-s' : 'u-indent-m' ) : 'u-indent-xl', size = '';

    if (length <= 2) {
        size = 'u-size-l';
    } else if (length <= 4) {
        size = 'u-size-m';
    } else {
        size = 'u-size-s';
    }

    return { indent, size };
}

function getFormattedText(text) {
    return text.includes('(') ? text.slice(0, text.indexOf('(')) : text;
}

export { fillMainWithMenu, fillMainWithServices, fillMainWithQuestion, fillMainWithUndefined, fillMainWithResults,
    fillMainWithUserParams }