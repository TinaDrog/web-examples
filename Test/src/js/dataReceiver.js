async function getInfo(query, callback) {
    try {
        return await new Promise(function (resolve, reject) {
            let server = new WebSocket('ws://office.interpay.com.ua:585/');

            server.onopen = function () {
                resolve(server);
                server.send(query);
            };
            server.onerror = function (err) {
                reject(server);
                console.log('error - ', err);
            };
            server.onmessage = function (event) {
                server.close();
                callback(event.data);
            }
        });
    } catch (error) {
        console.log("can't connect to server ", error)
    }
}

export { getInfo };