//= rigger/base/constants.js
//= rigger/base/enums.js
//= rigger/base/regexps.js
//= rigger/base/App.js

//= rigger/base/utils.js
//= rigger/base/words.js
//= rigger/base/functions.js
//= rigger/base/prototypes.js

//= rigger/data/ukr-keys.js
//= rigger/data/eng-keys.js
//= rigger/data/num-keys.js

//= components/input/input.js

//= components/scroller/scroller.js

//= components/input/input__reset.js
//= components/key/key.js
//= components/keyboard/keyboard.js

//= rigger/execute/main.js