var lastPageX = 0;

Array('touchstart', 'mousedown').forEach((type) => {
    App.hints.addEventListener(type, (evnt) => {
        lastPageX = evnt.pageX === undefined? evnt.changedTouches[0].pageX : evnt.pageX;
        App.hints.classList.add('hint-wrapper--moving')
    });
});

Array('touchmove', 'mousemove').forEach((type) => {
    document.addEventListener(type, (evnt) => {

        let x = evnt.pageX === undefined? evnt.changedTouches[0].pageX : evnt.pageX;

        if (App.hints.classList.contains('hint-wrapper--item-clicked')) {

            if (Math.abs(x - lastPageX) > 16) {
                let activeItem = document.querySelector('.hint__item--active');
                activeItem.classList.remove('hint__item--active');

                App.hints.classList.remove('hint-wrapper--item-clicked');
                App.hints.classList.add('hint-wrapper--moving')
            }
        }
        if (App.hints.classList.contains('hint-wrapper--moving')) {
            let preventScroll = App.hints.scrollLeft;

            if (x < lastPageX) {
                App.hints.scrollLeft = preventScroll + (lastPageX - x);
            } else {
                App.hints.scrollLeft = preventScroll - (x - lastPageX);
            }
            lastPageX = x;
        }
    });
});

Array('touchend', 'touchcancel', 'mouseup').forEach((type) => {
    document.addEventListener(type, (evnt) => {
        App.hints.classList.remove('hint-wrapper--moving');
        App.hints.classList.remove('hint-wrapper--item-clicked');
    });
});