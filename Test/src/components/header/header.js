import { getInfo } from "../../dataReceiver.js";

class TerminalData {
    constructor(id, posId, address, phones, orgType, isTest, isCash, isPos, serverVersion) {
        this.id = id;
        this.posId = posId;
        this.address = address;
        this.phones = phones;
        this.orgType = orgType;
        this.isTest = isTest;
        this.isCash = isCash;
        this.isPos = isPos;
        this.serverVersion = serverVersion;
    }
}

function terminalDataProcessing(data) {
    let parser = new DOMParser();
    let xmlDoc = parser.parseFromString(data, "text/xml");
    let tag = xmlDoc.querySelectorAll('GetTermInfo');

    let terminalData = new TerminalData(tag[0].attributes.term_id.value, tag[0].attributes.pos_term_id.value,
        tag[0].attributes.term_adr.value, tag[0].attributes.term_tel.value, tag[0].attributes.type_org.value,
        tag[0].attributes.istest.value, tag[0].attributes.iscash.value, tag[0].attributes.ispos.value,
        tag[0].attributes['svr-version']);

    fillPageWithHeader(terminalData.address, terminalData.phones);
}

function processPhones(phonesData) {
    let phones = phonesData.split(', ');

    for (let i in phones) {
        let phone = phones[i].replace(/[^\d]/g, '');
        phones[i] = '(' + phone.slice(0, 3) + ') ' + phone.slice(3, 6) + ' - ' + phone.slice(6, 8) + ' - ' + phone.slice(8);
    }

    return phones;
}

function fillPageWithHeader(terminalAddress, phoneNumbers) {
    let address = document.createElement('p');
    address.setAttribute('class', 'header__address');
    address.setAttribute('id', 'address');
    address.innerHTML = terminalAddress;

    let contacts = document.createElement('div');
    contacts.setAttribute('class', 'header__contacts');
    contacts.setAttribute('id', '_contacts');

    let contactsTitle = document.createElement('p');
    contactsTitle.setAttribute('class', 'header__contacts-title');
    contactsTitle.innerHTML = 'Телефони гарячої лінії:';

    contacts.appendChild(contactsTitle);

    phoneNumbers = processPhones(phoneNumbers);

    for (let i = 0; i < phoneNumbers.length; i++) {
        let phone = document.createElement('p');
        phone.setAttribute('class', 'header__contacts-phone');
        phone.setAttribute('id', String(i));
        phone.innerHTML = phoneNumbers[i];

        contacts.appendChild(phone);
    }

    let account = document.createElement('div');
    account.setAttribute('class', 'header__account');

    let accountTitle = document.createElement('p');
    accountTitle.setAttribute('class', 'header__account-title');
    accountTitle.innerHTML = 'Рахунок:';

    let accountAmount = document.createElement('p');
    accountAmount.setAttribute('class', 'header__account-amount');
    accountAmount.innerHTML = '0,00 грн';

    account.appendChild(accountTitle);
    account.appendChild(accountAmount);

    let header = document.getElementById('header');
    header.appendChild(address);
    header.appendChild(contacts);
    header.appendChild(account);
}

getInfo('<?xml version="1.0" encoding="utf-8"?><request lang="ua"><getterminfo/></request>', terminalDataProcessing);