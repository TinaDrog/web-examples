import { handlerBackClick, handlerMainClick, handlerNextClick } from "../main/main.js";

var clickEvents = {
    'Назад': handlerBackClick,
    'Головна': handlerMainClick,
    'Далі': handlerNextClick,
    'Оплата': null,
    'Оплатити': null
};

function fillPageWithFooter(back = null, main = null, next = null) {
    console.log('fillPageWithFooter');

    let footer = document.getElementById('footer');
    footer.innerHTML = '';

    let elements = ['back', 'main', 'next'];

    for (let i in arguments) {
        if (i >= elements.length) {
            break;
        }

        let footerElement = createFooterElement(elements[i], arguments[i], clickEvents[arguments[i]]);
        if (elements[i] === 'main' && next !== null) {
            footerElement.style.flex = '2';
        }
        footer.appendChild(footerElement);
    }
}

function createFooterElement(type, text, handlerClick) {
    let footerElement = document.createElement('button');
    footerElement.classList.add('footer__' + type);

    if (text === null) {
        footerElement.classList.add('u-d-none');
    }

    footerElement.setAttribute('id', type);
    footerElement.innerHTML = text;
    footerElement.addEventListener('click', handlerClick);

    return footerElement;
}

export { fillPageWithFooter }