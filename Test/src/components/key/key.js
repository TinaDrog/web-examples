
class Component {

    constructor() {
        this.events = [];
        this.dataset = {};
    }

    getObjectVars() {
        return Object.getOwnPropertyNames(this);
    }

    setAttributes(attr) {
        let vars = this.getObjectVars();
        Object.keys(attr).forEach((key) => {
            if (vars.includes(key)) {
                let setter = `set` + jsUcfirst(key);
                if (this[setter] !== 'undefined') {
                    this[setter](attr[key]);
                } else {
                    this[key] = attr[key];
                }

            }
        });
    }

    getAttributes() {
        let attributes = {};
        this.getObjectVars().forEach((_var) => {

            let getter = `get` + jsUcfirst(_var);
            if (this[getter] !== 'undefined') {
                attributes[_var] = this[getter]();
            } else {
                attributes[_var] = this[_var];
            }
        });
        return attributes
    }

    addData(key, value) {
        this.dataset[key] = value;
    }

    getData(key) {
        return this.dataset[key] || null;
    }

    /**
     *
     * @param {string }type
     * @param {function} listener
     */
    addEventListener(type, listener) {
        this.events.push({
            type: type,
            listener : listener
        })
    }

    layout() {
        return '<div></div>';
    }

    createElement() {
        return createElemByString(this.layout());
    }

    render() {
        let el = this.createElement();
        this.events.forEach((event) => {
            el.addEventListener(event.type, (evt) => { event.listener(evt); }, false);
        });
        return el;
    }
}


class Button extends Component {

    /**
     *
     * @param {BUTTON_TYPE_ENUM} type Тип кнопки
     * @param {ButtonView} view Содержимое кнопки
     * @param {number} items Занимаемое кол-во ячеек кнопкой
     */
    constructor(type, view, items = 1) {
        super();
        this.view = view;
        this.items = items;
        this.addData('type', type);

    }

    /**
     *
     * @param {ButtonView} view
     */
    setView(view) {
        this.view = view;
    }

    /**
     *
     * @returns {string}
     */
    layout() {
        let data = Object.keys(this.dataset).map((key) => {
            return `data-${key}="${this.dataset[key]}"`
        }).join(" ");

        return `<button class="key key--${this.dataset.type} key--${this.items}" type="button" ${data}>${this.view.layout()}</button>`
    }


}

class SimpleButton extends Button {
    /**
     *
     * @param {string} character
     * @param {ButtonView} view
     * @param {number} items Занимаемое кол-во ячеек кнопкой
     */
    constructor(character, view, items= 1) {
        super(BUTTON_TYPE_ENUM.CHARACTER, view, items);
        this.addData('character', character);
    }

    render() {
        let btn = super.render();
        btn.addEventListener('click', (evt) => {
            App.input.focus();
            let ch = evt.currentTarget.dataset.character;
            App.input.setValue(App.input.el.value.splice(App.input.position++, 0, ch), true);
            App.input.el.setSelectionRange(App.input.getPosition(), App.input.getPosition());
            hint();
        }, false);
        return btn;
    }
}

class CommandButton extends Button {
    /**
     *
     * @param command
     * @param {ButtonView} view
     * @param {number} items Занимаемое кол-во ячеек кнопкой
     */
    constructor(command, view, items = 1) {
        super(BUTTON_TYPE_ENUM.COMMAND, view, items);
        this.addData('command', command);
    }

    render() {
        let btn = super.render();
        let cmd = this.getData('command');

        btn.addEventListener('click', (evt) => {
            App.commands[cmd](evt);
        }, false);
        return btn;
    }
}


class ButtonView {
    constructor(content) {
        this.content = content;
    }

    layout() {
        return this.content;
    }
}

class CharacterButtonView extends ButtonView{

    constructor(content) {
        super(content);
        this.class = "key__inner"
    }

    layout() {
        return `<span class="${this.class}">${this.content}</span>`;
    }
}

class SvgButtonView extends ButtonView {

    constructor(svg) {
        super(svg);
        this.class = "key__inner key__inner--svg"
    }

    layout() {
        if (this.content.startsWith('#')) {
            return `<span class="${this.class}">
                <svg><use xlink:href="${this.content}"></use></svg>
            </span>`;
        } else {
            return super.layout();
        }
    }
}

class ImgButtonView extends ButtonView {

    constructor(path) {
        super(path);
        this.class = "key__inner key__inner--img"
    }

    layout() {
        return `<img src="${this.content}" alt=""/>`
    }
}

class ButtonViewCreator {
    static forCharacterInstance(content) {
        return new CharacterButtonView();
    }

    static forSvgInstance(content) {
        return new SvgButtonView(content);
    }

    static forImageInstance(content) {
        return new ImgButtonView(content);
    }
}