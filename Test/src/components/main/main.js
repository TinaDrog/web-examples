import { getInfo } from "../../dataReceiver.js";
import { fillMainWithMenu, fillMainWithServices, fillMainWithQuestion, fillMainWithUndefined,
    fillMainWithResults, fillMainWithUserParams } from "../../filler.js";


// global variables
var modules = [], path = [];
var terminalID, currentQuestion = 0;
var structure, currentStructure;


// classes
class ModuleData {
    constructor(progName, menuText, id, serviceGroups) {
        this.progName = progName;
        this.menuText = menuText;
        this.id = id;
        this.serviceGroups = serviceGroups;
    }
}

class ServiceGroup {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}

class Question {
    constructor(fullText, briefText, id, answers) {
        this.fullText = fullText;
        this.briefText = briefText;
        this.id = id;
        this.answers = answers;
    }
}

class Answer {
    constructor(text, brief, id, equeryJoinId, question, results) {
        this.text = text;
        this.brief = brief;
        this.id = id;
        this.equeryJoinId = equeryJoinId;
        this.question = question;
        this.results = results;
    }

}

class Results {
    constructor(serviceName, serviceID, userParams) {
        this.serviceName = serviceName;
        this.serviceID = serviceID;
        this.userParams = userParams;
    }
}

class UserParams {
    constructor(name, type, mandatory, listPlaceholder, continuos, placeholder, prompt1, prompt2, inputHint, correctExample, mainPrompt, inputMask, regExpr, fixedValue) {
        this.name = name;
        this.type = type;
        this.mandatory = mandatory;
        this.listPlaceholder = listPlaceholder;
        this.continuos = continuos;
        this.placeholder = placeholder;
        this.prompt1 = prompt1;
        this.prompt2 = prompt2;
        this.inputHint = inputHint;
        this.correctExample = correctExample;
        this.mainPrompt = mainPrompt;
        this.inputMask = inputMask;
        this.regExpr = regExpr;
        this.fixedValue = fixedValue;
    }
}


// data handlers
function terminalDataProcessing(data) {
    console.log('------------------------------------');
    console.log('terminalDataProcessing');
    console.log(data);
    let parser = new DOMParser();
    let xmlDoc = parser.parseFromString(data, "text/xml");

    if (xmlDoc.querySelectorAll('answer')[0].innerHTML !== 'getterminfo') {
        console.log('ne to');
        getInfo('<?xml version="1.0" encoding="utf-8"?><request lang="ua"><getterminfo/></request>', terminalDataProcessing);
    }

    let tag = xmlDoc.querySelectorAll('GetTermInfo');

    terminalID = tag[0].attributes.term_id.value;
    getInfo('<?xml version="1.0" encoding="utf-8"?><request lang="ua"><menu_xml terminal_id="' + '140103' + '"/></request>',
        menuDataProcessing);
    /*getInfo('<?xml version="1.0" encoding="utf-8"?><request lang="ua"><menu_xml terminal_id="' + terminalID + '"/></request>',
       menuDataProcessing);*/
}

function menuDataProcessing(data) {
    console.log('------------------------------------');
    console.log('menuDataProcessing');
    console.log(data);
    let parser = new DOMParser();
    let xmlDoc = parser.parseFromString(data, "text/xml");

    let moduleTags = xmlDoc.querySelectorAll('module');

    for (let moduleTag of moduleTags) {
        let services = moduleTag.children[3].children;
        let serviceGroups = [];

        for (let service of services) {
            serviceGroups.push(new ServiceGroup(service.attributes.id.value, service.firstElementChild.innerHTML));
        }

        let moduleData = new ModuleData(moduleTag.children[0].innerHTML, moduleTag.children[1].innerHTML,
            moduleTag.children[2].innerHTML, serviceGroups);

        modules.push(moduleData);
    }

    fillMainWithMenu(modules, handlerMenuClick);
}

function servicesDataProcessing(data) {
    console.log('------------------------------------');
    console.log('servicesDataProcessing');
    console.log(data);
    let parser = new DOMParser();
    let xmlDoc = parser.parseFromString(data, "text/xml");

    structure = getQuestion(xmlDoc, 'response > question');
    console.log(xmlDoc.querySelectorAll('results'));

    console.log(structure[currentQuestion]);

    structure !== null ? fillMainWithQuestion(path, structure[currentQuestion], handlerAnswerClick) : fillMainWithUndefined();
    currentStructure = structure;
}


// parsers
function getQuestion(data, query) {
    let questionTags = data.querySelectorAll(query);
    if (questionTags.length === 0) {
        return null;
    }

    let rootQuestions = [];

    for (let questionTag of questionTags) {
        let answers = getAnswers(questionTag.children[3], query);
        let question = new Question(questionTag.children[0].innerHTML, questionTag.children[1].innerHTML,
            questionTag.children[2].innerHTML, answers);

        rootQuestions.push(question);
    }

    return rootQuestions;
}

function getAnswers(data, query) {
    query += ' > answers > answer';
    let answerTags = data.querySelectorAll(query);
    let result = [];

    for (let answerTag of answerTags) {
        let results = getResults(answerTag, query + ' > results');
        let question = getQuestion(answerTag, query + ' > question');
        let answer = new Answer(answerTag.getAttribute('text'), answerTag.getAttribute('brief'),
            answerTag.getAttribute('id'), answerTag.getAttribute('equery_join_id'), question, results);

        result.push(answer);
    }

    return result;
}

function getResults(data, query) {
    let resultsTag = data.querySelectorAll(query);
    if (resultsTag.length === 0) {
        return null;
    }

    let resultsData = resultsTag[0].children;

    if (resultsData.length === 1) {
        let userParams = getUserParams(resultsData[0], query);
        return new Results(null, null, userParams);
    } else if (resultsData.length === 2) {
        console.log('server-option processing');
    } else {
        let userParams = getUserParams(resultsData[2], query);
        return new Results(resultsData[0].innerHTML, resultsData[1].innerHTML, userParams);
    }
}

function getUserParams(data) {
    let userParamsTags = data.querySelectorAll('userParam');
    if (userParamsTags.length === 0) {
        return null;
    }

    let result = [];

    for (let userParamsTag of userParamsTags) {
        let userParams = new UserParams(userParamsTag.getAttribute('name'), userParamsTag.getAttribute('type'),
            userParamsTag.getAttribute('mandatory'), userParamsTag.getAttribute('listPlaceholder'),
            userParamsTag.getAttribute('continuos'), userParamsTag.getAttribute('placeholder'),
            userParamsTag.getAttribute('prompt1'), userParamsTag.getAttribute('prompt2'),
            userParamsTag.getAttribute('inputHint'), userParamsTag.getAttribute('correctExample'),
            userParamsTag.getAttribute('mainPrompt'), userParamsTag.getAttribute('inputMask'),
            userParamsTag.getAttribute('regExpr'), userParamsTag.getAttribute('fixedValue'));
        result.push(userParams);
    }

    return result;
}


// handlers
// лежить тут, а не в filler.js, щоб зберігать дані в path
function handlerMenuClick(event) {
    let module = getModuleById(event.target.getAttribute('id'));

    path.push({
        type: 'menu',
        id: event.target.getAttribute('id'),
        text: module.menuText,
        questionNumber: 0
    });

    fillMainWithServices(path, modules, handlerServiceClick);
}

function handlerServiceClick(event) {
    let element = event.target;

    path.push({
        type: 'service',
        id: event.target.children[0].getAttribute('id'),
        text: element.children[0].innerHTML,
        questionNumber: 0
    });

    getInfo('<?xml version="1.0" encoding="utf-8"?><request lang="ua"><scenario_xml id="'
         + (parseInt(path[path.length - 1].id)) + '" terminal_id="' + terminalID + '"/></request>', servicesDataProcessing);
}

function handlerAnswerClick(answerData, questionData) {
    path.push({
        type: 'answer',
        id: answerData.id,
        text: answerData.text,
        previousQuestion: questionData,
        questionNumber: currentQuestion
    });

    if (answerData.results !== null) {
        fillMainWithUserParams(answerData.results.userParams[0]);
        return;
    }

    // якшо є вкладене питання, то ставимо його; якшо немає - ставимо наступне кореневе питання; якшо кореневі питання закінчились - результат
    if (answerData.question !== null) {
        fillMainWithQuestion(path, answerData.question[0], handlerAnswerClick);
    } else {
        currentQuestion++;
        if (currentQuestion < structure.length) {
            fillMainWithQuestion(path, structure[currentQuestion], handlerAnswerClick);
        } else {
            console.log('do not have question');
            console.log(answerData.results);
            fillMainWithResults(path, handlerEditClick);
        }
    }
}

function handlerEditClick(pathElement) {
    let index = path.indexOf(pathElement);
    path = path.slice(0, index + 1);

    currentQuestion = path[path.length - 1].questionNumber;
    fillMainWithQuestion(path, path[path.length - 1].previousQuestion, handlerAnswerClick);
    path.pop();
}

function handlerBackClick() {
    let lastPathEl = path[path.length - 1];

    currentQuestion = lastPathEl.questionNumber;

    if (lastPathEl.type === 'menu') {
        fillMainWithServices(path, modules, handlerServiceClick);
    } else if (lastPathEl.type === 'service') {
        path.pop();
        fillMainWithServices(path, modules, handlerServiceClick)
    } else if (lastPathEl.type === 'answer') {
        path.pop();
        fillMainWithQuestion(path, lastPathEl.previousQuestion, handlerAnswerClick);
        //path.pop();
    }
}

function handlerMainClick() {
    path = [];
    currentQuestion = 0;

    fillMainWithMenu(modules, handlerMenuClick);
}

function handlerNextClick() {
    console.log('here');
    currentQuestion++;
    if (currentQuestion < structure.length) {
        fillMainWithQuestion(path, structure[currentQuestion], handlerAnswerClick);
    } else {
        console.log('do not have question');
        fillMainWithResults(path, handlerEditClick);
    }
}

// helpers
function getModuleById(id) {
    for (let module of modules) {
        if (module.id === id) {
            return module;
        }
    }
}

/*document.addEventListener('click', function() {
    console.log('path - ', path);
    console.log('currQ - ', currentQuestion);
});*/

getInfo('<?xml version="1.0" encoding="utf-8"?><request lang="ua"><getterminfo/></request>', terminalDataProcessing);

export { handlerMainClick, handlerBackClick, handlerNextClick }
