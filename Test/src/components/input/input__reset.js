addEvent(
    document.querySelector('.input__reset'),
    'click',
    (evnt) => {
        evnt.preventDefault();
        App.input.clear();
        hint();
        App.input.dispatchEvent(App.inputChange);
    }
);