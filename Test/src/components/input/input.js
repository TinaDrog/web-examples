class Input {
    /**
     *
     * @param {Node} element
     */
    constructor(element) {
        this.el = element;
        this.position = 0;
        this.value = '';
        this.matchEvent = new Event('match');

        Array('click', 'keydown', 'input').forEach((type) => {
            this.el.addEventListener(type, (evnt) => {
                App.input.setValue(evnt.currentTarget.value);
                App.input.setPosition(evnt.currentTarget.selectionEnd, true);
                hint();
            })
        });

        this.el.addEventListener('select', function() {
            this.selectionStart = this.selectionEnd;
        }, false);

        this.el.addEventListener('match', (evnt) => {

            if (REGEXP.fio.test(this.value)) {
                App.input.el.classList.remove('input__field--error');
                App.input.el.classList.add('input__field--success');
            } else {
                App.input.el.classList.remove('input__field--success');
                App.input.el.classList.add('input__field--error');
            }
        });
    }

    setValue(value, needRefresh = false) {
        this.el.value = this.value = value;
        if (needRefresh) {
            this.refresh();
        }
    }

    getValue() {
        return this.value;
    }

    setPosition(position, needRefresh = false) {
        this.el.position = this.position = position;
        if (needRefresh) {
            this.refresh();
        }
    }

    getPosition() {
        return this.position;
    }

    backPosition() {
        if (this.position > 1) {
            this.position--;
            this.refresh();
        }
    }

    forwardPosition() {
        if (this.position < this.value.length) {
            this.position++;
            this.refresh();
        }
    }

    deleteSymbol() {
        this.value    = this.value.delC(this.position - 1, 1);
        this.position = (this.position > 1) ? this.position - 1 : 0;
        this.refresh();
    }

    refresh() {
        this.el.value = this.value;
        this.el.position = this.position;
        this.el.setSelectionRange(this.position, this.position);
        this.match();
        this.focus();
    }

    clear() {
        this.setPosition(0);
        this.setValue('');
        this.refresh();
    }

    focus() {
        this.el.focus();
    }

    match() {
        this.el.dispatchEvent(this.matchEvent);
    }
}
