var numKeys = () => {
    return [
        [
            '1',
            '2',
            '3',
            {
                svg:'#delete-icon',
                c:'delete'
            }
        ],
        [
            '4',
            '5',
            '6',
            {
                s:'←',
                c:'back'
            },
        ],
        [
            '7',
            '8',
            '9',
            {
                s:'→',
                c:'forward'
            },
        ],
        [
            {
                s:'0',
                i:3
            },
            {
                svg:'#earth-icon',
                c:'toSymbols'
            },
        ],
    ]
}


