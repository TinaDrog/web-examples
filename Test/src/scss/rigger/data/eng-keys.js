var engKeys = () => {
    return [
        [
            'Q',
            'W',
            'E',
            'R',
            'T',
            'Y',
            'U',
            'I',
            'O',
            'P',
            {
                svg:'#delete-icon',
                c:'delete'
            }
        ],
        [
            'A',
            'S',
            'D',
            'F',
            'G',
            'H',
            'J',
            'K',
            'L',
            {
                s: '/',
                i: 2
            }

        ],
        [
            'Z',
            'X',
            'C',
            'V',
            'B',
            'N',
            'M',
            '-',
            '.',
            {
                s:'\\',
                i: 2
            }

        ],
        [
            '*',
            '№',
            {
                s:'←',
                c:'back'
            },
            {
                s:' ',
               // c:'space',
                i: 4
            },
            {
                s:'→',
                c:'forward'
            },
            {
                svg:'#numbers-icon',
                c:'toNumbers'
            },
            {
                svg:'#earth-icon',
                c:'toUkr'
            },

        ]

    ]
}