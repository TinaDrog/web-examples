/**
 *
 * @param s Исходная строка
 * @param ch Символ разбиения
 * @param pos
 * @returns {{len: number, left: number, words: [], ind: number}}
 */
function split(s, ch, pos) {
    let res = [];
    let ind = -1;
    let resLeft = -1;
    let resLen = 0;

    for (let i = 0, length = s.length, word = '', left = -1, len = 0;
         i < length;
         ++i) {
        if (s[i] !== ch) {
            if (left < 0) {
                left = i;
            }
            ++len;
            word += s[i];
        }
        if (s[i] === ch || ((i + 1) === length)) {
            if (word.length > 0) {
                res.push(word);
                if (left <= pos && (left + len >= pos)) {
                    resLeft = left;
                    resLen = len;
                    ind = res.length - 1;
                }

                left = -1;
                len = 0;
                word = '';
            }
        }
    }

    return { words: res, ind: ind, left: resLeft, len: resLen }
}

function fioHints(word, ind, cntMax = 25) {
    let res = [];
    if (!filesReady) return res;
    word = word.toLocaleUpperCase();
    let i = listEnter(App.wordList[ind], word);

    while (App.wordList[ind][i].w.indexOf(word) === 0) {
        res.push(App.wordList[ind][i]);
        i++;
    }
    res=res.sort((a, b) => b.s - a.s);
    res = res.map((el)=>el.w);
    return res.slice(0,cntMax);
}

function listEnter(list, w, i = 0, j = list.length) {
    if ((j - i) < 2) return (i == j) ? i : w > list[i].w ? j : i
    let l = ((i + j) / 2) >> 0
    let ii, jj
    if (w < list[l].w) { jj = l; ii = i } else { ii = l; jj = j }
    return listEnter(list, w, ii, jj)
}

function hint() {
    App.hints.innerHTML = '';

    let res = split(App.input.value, ' ', App.input.position);

    if (res.ind < 0 || res.ind >= res.words.length) {
        return;
    }

    let word = res.words[res.ind];
    let words = fioHints(word, res.ind);
    if (words.length > 0) {
        words.forEach((el) => {
            let btn = createElemByString(`<button class="hint__item" type="button">${el}</button>`);
            let li = createElemByString(`<li class="hint" "></li>`);

            btn.addEventListener('click', (event) => {

                if (event.currentTarget.classList.contains(activeScrollClass)) {
                    event.currentTarget.classList.remove(activeScrollClass);
                    App.hints.innerHTML = '';
                    let ins = event.currentTarget.innerText + (res.ind !== 2? ' ' : '');
                    if (res.left >= 0) {
                        App.input.setValue(App.input.value.splice(res.left, (res.ind !== 2? res.len+1 : res.len), ins));
                        App.input.setPosition(res.left + ins.length);
                        App.input.el.setSelectionRange(App.input.position, App.input.position);
                    }
                }
            }, true);

            li.appendChild(btn);
            App.hints.appendChild(li);
        })
    }
}