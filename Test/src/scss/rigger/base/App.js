var App = {
    keyboard : null,
    input    : null,
    hints    : null,

    wordList : [],

    /**
     *
     * @param settings Ассоц. массив настроек
     */
    init(settings) {
        let self     = this;
        let input    = document.getElementById('input');
        let keyboard = document.getElementById('keyboard');
        let hints    = document.getElementById('hints');

        if (input) {
            self.input = new Input(input);
        }

        if (keyboard) {
            self.keyboard = new Keyboard(keyboard, 12);
        }

        if (hints) {
            self.hints = hints;
        }
    },

    commands : {
        back: (evnt) => {
            App.input.backPosition();
            hint();
        },
        forward: (evnt) => {
            App.input.forwardPosition();
            hint();
        },
        delete : (evnt) => {
            App.input.deleteSymbol();
            hint();

        },
        space : (evnt) => {

        },
        toSymbols: (evnt) => {
            App.keyboard.reverseKeyboard();
        },
        toEng: (evnt) => {
            App.keyboard.changeKeyboard(KEYBOARD_TYPE.ENG);
        },
        toUkr: (evnt) => {
            App.keyboard.changeKeyboard(KEYBOARD_TYPE.UKR);
        },
        toNumbers: (evnt) => {
            App.keyboard.changeKeyboard(KEYBOARD_TYPE.UKR);
        }
    }
};
