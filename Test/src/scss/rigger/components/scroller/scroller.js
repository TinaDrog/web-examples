var lastPageX = 0;

document.querySelectorAll('.horizontal-scroller').forEach((scroller) => {
    Array('touchstart', 'mousedown').forEach((type) => {
        scroller.addEventListener(type, (evnt) => {
            lastPageX = evnt.pageX === undefined? evnt.changedTouches[0].pageX : evnt.pageX;

            if (evnt.target !== evnt.currentTarget) {
                evnt.target.classList.add(activeScrollClass);
                scroller.classList.add(innerClickedClass);
            } else {
                scroller.classList.add(movingScrollClass)
            }
        });
    });

    Array('touchmove', 'mousemove').forEach((type) => {
        document.addEventListener(type, (evnt) => {

            let x = evnt.pageX === undefined? evnt.changedTouches[0].pageX : evnt.pageX;

            if (scroller.classList.contains(innerClickedClass)) {

                if (Math.abs(x - lastPageX) > 16) {

                    let activeItem = document.querySelector(`.${activeScrollClass}`);
                    activeItem && activeItem.classList.remove(activeScrollClass);

                    scroller.classList.remove(innerClickedClass);
                    scroller.classList.add(movingScrollClass)
                }
            }
            if (scroller.classList.contains(movingScrollClass)) {
                let preventScroll = scroller.scrollLeft;

                if (x < lastPageX) {
                    scroller.scrollLeft = preventScroll + (lastPageX - x);
                } else {
                    scroller.scrollLeft = preventScroll - (x - lastPageX);
                }
                lastPageX = x;
            }
        });
    });

    Array('touchend', 'touchcancel', 'mouseup', 'click').forEach((type) => {
        document.addEventListener(type, (evnt) => {
            scroller.classList.remove(movingScrollClass);
            scroller.classList.remove(innerClickedClass);
            if (evnt.type === 'click') {
                document.querySelectorAll(`.${activeScrollClass}`).forEach((el) => {
                    el.classList.remove(activeScrollClass);
                })
            }
        }, false);
    });
});