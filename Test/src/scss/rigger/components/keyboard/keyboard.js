
class Keyboard {
    /**
     *
     * @param element
     * @param itemsPerRow
     */
    constructor(element, itemsPerRow) {
        this.el = element;
        this.itemsPerRow  = itemsPerRow;
        this.currentType  = KEYBOARD_TYPE.UKR;
        this.previousType = KEYBOARD_TYPE.UKR
    }

    /**
     *
     * @param type {KEYBOARD_TYPE} type
     * @returns {null|Array} keys
     */
    getKeys(type) {
        let keys = null;
        switch (type) {
            case KEYBOARD_TYPE.UKR:
                keys = ukrKeys();
                break;
            case KEYBOARD_TYPE.RUS:
                keys = rusKeys();
                break;
            case KEYBOARD_TYPE.ENG:
                keys = engKeys();
                break;
            case KEYBOARD_TYPE.NUM:
                keys = numKeys();
                break;
        }
        return keys;
    }

    /**
     *
     * @param {KEYBOARD_TYPE} type
     */
    changeKeyboard(type) {
        this.previousType = this.currentType;
        this.currentType  = type;
        this.buildKeyboard();
    }

    reverseKeyboard() {
        this.previousType = this.currentType;
        this.buildKeyboard();
    }

    buildKeyboard() {
        let keys = this.getKeys(this.currentType);

        if (!keys) {
            return;
        }

        this.clear();

        for(let i = 0, len = keys.length; i < len; ++i) {

            let line = createElemByString(`<div class="key-line key-line--${i + 1}"></div>`);

            for (let j = 0, len = keys[i].length; j < len; ++j) {

                let btn, key = keys[i][j], layout = null;

                if (typeof key == "object") {
                    if ('svg' in key) {
                        layout = new SvgButtonView(key.svg)
                    }

                    if ('c' in key) {
                        btn = new CommandButton(
                            key.c,
                            layout || new CharacterButtonView(key.s),
                            key.i || 1
                        );
                    } else {
                        btn = new SimpleButton(
                            key.s,
                            layout || new CharacterButtonView(key.s),
                            key.i || 1
                        );
                    }
                } else {
                    btn = new SimpleButton(
                        key,
                        new CharacterButtonView(key),
                        1
                    );
                }
                line.appendChild(btn.render());
            }
            this.el.appendChild(line);
        }
    }

    clear() {
        this.el.innerHTML = '';
    }



}