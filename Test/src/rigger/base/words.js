var filesReady = false;
let filesLoaded = 0,
    xhr = [];

Array(
    '/dist/assets/files/1.txt',
    '/dist/assets/files/2.txt',
    '/dist/assets/files/3.txt',
    '/dist/assets/files/fcnt.txt'
).forEach((fn, i) => {
    xhr[i] = new XMLHttpRequest();
    xhr[i].open('GET', fn, true);
    xhr[i].send();
    xhr[i].onload = () => {
        // console.log(i)
        App.wordList[i] = xhr[i].responseText.split(/\r?\n/)
            .map((el) => { return { w: el.toLocaleUpperCase(), s: 0 } });

        if (i > 2) {
            App.wordList[i]=App.wordList[i].map((el) => {
                let tmpW = el.w.split(';');
                return {w: tmpW[0], s: tmpW[1]}
            });
        }

        App.wordList[i].sort((a, b) => b.w < a.w ? 1 : -1);

        filesLoaded++;
        if (filesLoaded === 4) {
            filesReady = true;
            App.wordList[0]=App.wordList[3]
        }
    }
});