/**
 *
 * @param idx
 * @param rem
 * @param str
 * @returns {string}
 */
String.prototype.splice = function (idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

/**
 *
 * @param idx
 * @param rem
 * @returns {string}
 */
String.prototype.delC = function (idx, rem) {
    return this.slice(0, idx) + this.slice(idx + Math.abs(rem));
};