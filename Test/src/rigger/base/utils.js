/**
 * Создание DOM-элемента из строки
 * @param string HTML-код элемента
 * @returns {ChildNode}
 */
function createElemByString(string) {
    let template = document.createElement('template');
    template.innerHTML = string;
    return template.content.firstChild;
}

/**
 * Преобразует первый символ строки в верхний регистр
 * @param string Входная строка
 * @returns {string} Преобразованная срока
 */
function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 *
 * @param el
 * @param event
 * @param callback
 * @param isCapture
 */
function addEvent(el, event, callback, isCapture = false) {
    if (!el || !event || !callback || typeof callback !== 'function') return

    if (typeof el === 'string') {
        el = document.querySelector(el)
    }
    el.addEventListener(event, callback, isCapture)
}
