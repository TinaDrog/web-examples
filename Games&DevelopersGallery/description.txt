На сайті показано проекти (ігри) розробників. Є можливість реєстрації, редагування інформації, додавання свого проекту тощо. 
Використано: Node.js + Express, MongoDB + Mongoose, handlebars, jQuery, JS, CSS. Реалізовано MVC-патерн.
Сайт частково адаптований. Основна ціль - показати навички використання вище перелічених технологій.

-------------------------------------------------------------------------------------------------------------------------------
This website shows delevopers' projects (games). You have an ability to log in/log out, edit information, add a new project etc.
Technologies: Node.js + Express, MongoDB + Mongoose, handlebars, jQuery, JS, CSS. MVC-pattern was realized.
This website is partly adapted. The main goal is to show my skills in using technologies mentioned above. 