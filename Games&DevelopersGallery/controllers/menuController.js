const model = require("../models/model");

// get-requests

exports.admin = function (req, res) {
    res.render("admin.hbs");
};

// post-requests

exports.checkLogin = async function (req, res) {
    res.send( await model.checkLogin(req.body["login"]) );
};

exports.checkPassword = async function (req, res) {
    let user = await model.checkPassword(req.body["login"], req.body["password"]);

    res.cookie("login", user === "" ? "Guest" : req.body["login"], { maxAge: 900000, httpOnly: true });
    res.send(user);
};

exports.userInfo = function (req, res) {
    res.cookie("login", req.body["login"]);
    res.render("questionnaire.hbs", { login: req.body["login"] });
};