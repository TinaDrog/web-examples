const model = require("../models/model");

// get-requests

exports.getUsersData = async function (req, res) {
    res.send( await model.getUsersData() );
};

exports.getAppsData = async function (req, res) {
    let apps = await model.getAppsData();
    for (let app of apps) {
        app["likes"] = app["likes"].length;
    }
    res.send(apps);
};

// post-requests

exports.updateUserData = async function (req, res) {
    res.send( await model.updateUserData(req.body["data"]) );
};

exports.updateAppData = async function (req, res) {
    res.send( await model.updateAppData(req.body["data"]) );
};

exports.deleteUserData = async function (req, res) {
    res.send( await model.deleteUserData(req.body["id"]) );
};

exports.deleteAppData = async function (req, res) {
    res.send( await model.deleteAppData(req.body["id"]) );
};
