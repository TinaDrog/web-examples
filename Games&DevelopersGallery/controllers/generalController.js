const model = require("../models/model");

let login = undefined;

exports.menu = function (req, res) {
    res.cookie("login", undefined);
    res.render("menu.hbs");
};

exports.main = async function (req, res) {
    if (req.cookies["login"] === "Admin") {
        res.redirect("/admin");
        return;
    }
    res.cookie("login", req.cookies["login"] === "undefined" ? login : req.cookies["login"],
        { maxAge: 900000, httpOnly: true });

    res.render("main.hbs", { nickname: req.cookies["login"]});
};

exports.myApps = function (req, res) {
    res.render("myApps.hbs");
};

exports.insert = async function (req, res) {
    await model.addUser(req.body.login, req.body.password);
};

exports.updateInfo = async function (req, res) {
    login = req.body["login"];
    res.cookie("login", req.body["login"]);

    await model.updateInfo(req.body["login"], req.body["fullName"], req.body["nickname"], req.body["email"],
        req.body["phone"], req.body["bio"]);
};

/*function upload (targetPath, path, modelFunction, parameter, link, req, res) {
    let tmpPath = req.file.path;
    targetPath = targetPath + req.file.originalname;

    let src = fs.createReadStream(tmpPath);
    let dest = fs.createWriteStream(targetPath);

    src.pipe(dest);
    src
        .on('end', async function() {
            await modelFunction(parameter, path + req.file.originalname);
            res.redirect("/" + link);
        })
        .on('error', function(err) {
            res.send(err);
        });
}

async function makeData (apps) {
    let data = [];

    for (let app of apps) {
        let developer = await model.getUserById(app["developerID"]);
        data.push({ appId: app["_id"], appName: app["appName"], developer: developer["nickname"], likes: app["likes"],
            imgSrc: app["imgSrc"], downloadLink: app["downloadLink"]});
    }

    return data;
}

exports.admin = function (req, res) {
    res.render("admin.hbs");
};

exports.exit = function (req, res) {
    res.cookie("login", undefined, { maxAge: 900000, httpOnly: true });
    res.render("menu.hbs", {});
};

exports.edit = async function (req, res) {
    let currentUser = await model.getUser(req.cookies["login"]);

    res.render("edit.hbs", {
        login: currentUser["login"],
        fullName: currentUser["fullName"],
        nickname: currentUser["nickname"],
        email: currentUser["email"],
        phone: currentUser["phone"],
        bio: currentUser["bio"],
        imgSrc: currentUser["imgSrc"]
    });
};

exports.checkLogin = async function (req, res) {
    res.send( await model.checkLogin(req.body["login"]) );
};

exports.checkPassword = async function (req, res) {
    let user = await model.checkPassword(req.body["login"], req.body["password"]);

    res.cookie("login", user === "" ? "Guest" : req.body["login"], { maxAge: 900000, httpOnly: true });
    res.send(user);
};

exports.userInfo = function (req, res) {
    res.render("questionnaire.hbs", { login: req.body["login"] });
};

exports.uploadAvatar = async function (req, res) {
    upload("./public/uploads/images/", "../uploads/images/", model.insertImage, req.cookies["login"], "edit", req, res);
};

exports.addApp = async function (req, res) {
    await model.addApp( req.cookies["login"], req.body["appName"], req.body["developerID"], req.body["description"],
        req.body["imgSrc"], req.body["downloadLink"] );
};

exports.deleteApp = async function (req, res) {
    res.send( await model.deleteApp(req.body["appName"]) );
};

exports.addAppInfo = async function (req, res) {
    res.send( await model.getAppInfo(req.body["appName"]) );
};

exports.getAppsNames = async function (req, res) {
    let apps = await model.getAppsNames(req.cookies["login"]);
    let names = [];

    for (let app of apps) {
        names.push(app["appName"]);
    }

    res.send(names);
};

exports.uploadAppImage = async function (req, res) {
    upload("./public/uploads/images/", "../uploads/images/", model.insertAppImage,
        req.body["appName"], "myApps", req, res);
};

exports.uploadAppFile = async function (req, res) {
    upload("./public/uploads/applications/", "./uploads/applications/", model.uploadAppFile,
        req.body["appName"], "myApps", req, res);
};

exports.resetImage = async function (req, res) {
    await model.insertAppImage(req.body["appName"], "./images/appDefault.jpg");
};

exports.updateAppInfo = async function (req, res) {
    await model.updateAppInfo(req.body["currentApp"], req.body["appName"], req.body["description"],
        req.body["downloadLink"]);
};

exports.getTopUsersList = async function (req, res) {
    let users = await model.getTopUsersList();
    let data = [];

    for (let user of users) {
        data.push({ nickname: user["nickname"], likes: user["likes"], applicationsAmount: user["applicationsAmount"],
            imgSrc: user["imgSrc"]});
    }

    res.send(data);
};

exports.getTopAppsList = async function (req, res) {
    res.send( await makeData( await model.getTopAppsList() ) );
};

exports.getLikedApps = async function (req, res) {
    res.send( await model.getLikedApps(req.cookies["login"]) )  ;
};

exports.like = async function (req, res) {
    res.send( await model.like(req.body["appId"], req.cookies["login"]) );
};

exports.download = async function (req, res) {
    console.log(req.body["appId"]);
    await model.addDownloads(req.body["appId"]);
    await res.download("./public/images/Back.png");
};

exports.getUserApps = async function (req, res) {
    res.send( await model.getUserApps(req.body["nickname"]) );
};

exports.getAppsByName = async function (req, res) {
    res.send( await makeData( await model.getAppsByName(req.body["name"]) ) );
};

exports.getUsersData = async function (req, res) {
    res.send( await model.getUsersData() );
};

exports.getAppsData = async function (req, res) {
    let apps = await model.getAppsData();
    for (let app of apps) {
        app["likes"] = app["likes"].length;
    }
    res.send(apps);
};

exports.updateUserData = async function (req, res) {
    res.send( await model.updateUserData(req.body["data"]) );
};

exports.updateAppData = async function (req, res) {
    res.send( await model.updateAppData(req.body["data"]) );
};

exports.deleteUserData = async function (req, res) {
    res.send( await model.deleteUserData(req.body["id"]) );
};

exports.deleteAppData = async function (req, res) {
    res.send( await model.deleteAppData(req.body["id"]) );
};*/