const model = require("../models/model"),
    fs = require("fs");

function upload (targetPath, path, modelFunction, parameter, link, req, res) {
    let tmpPath = req.file.path;
    targetPath = targetPath + req.file.originalname;

    let src = fs.createReadStream(tmpPath);
    let dest = fs.createWriteStream(targetPath);

    src.pipe(dest);
    src
        .on('end', async function() {
            await modelFunction(parameter, path + req.file.originalname);
            res.redirect("/" + link);
        })
        .on('error', function(err) {
            res.send(err);
        });
}

// post-requests

exports.addApp = async function (req, res) {
    await model.addApp( req.cookies["login"], req.body["appName"], req.body["developerID"], req.body["description"],
        req.body["imgSrc"], req.body["downloadLink"] );
};

exports.deleteApp = async function (req, res) {
    res.send( await model.deleteApp(req.body["appName"]) );
};

exports.addAppInfo = async function (req, res) {
    res.send( await model.getAppInfo(req.body["appName"]) );
};

exports.uploadAppImage = async function (req, res) {
    upload("./public/uploads/images/", "../uploads/images/", model.insertAppImage,
        req.body["appName"], "myApps", req, res);
};

exports.uploadAppFile = async function (req, res) {
    upload("./public/uploads/applications/", "./uploads/applications/", model.uploadAppFile,
        req.body["appName"], "myApps", req, res);
};

exports.resetImage = async function (req, res) {
    await model.insertAppImage(req.body["appName"], "./images/appDefault.jpg");
};

exports.updateAppInfo = async function (req, res) {
    await model.updateAppInfo(req.body["currentApp"], req.body["appName"], req.body["description"],
        req.body["downloadLink"]);
};