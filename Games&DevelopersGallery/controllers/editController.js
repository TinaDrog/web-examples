const model = require("../models/model"),
    fs = require("fs");

async function makeData (apps) {
    let data = [];

    for (let app of apps) {
        let developer = await model.getUserById(app["developerID"]);
        data.push({ appId: app["_id"], appName: app["appName"], developer: developer["nickname"], likes: app["likes"],
            imgSrc: app["imgSrc"], downloadLink: app["downloadLink"]});
    }

    return data;
}

function upload (targetPath, path, modelFunction, parameter, link, req, res) {
    let tmpPath = req.file.path;
    targetPath = targetPath + req.file.originalname;

    let src = fs.createReadStream(tmpPath);
    let dest = fs.createWriteStream(targetPath);

    src.pipe(dest);
    src
        .on('end', async function() {
            await modelFunction(parameter, path + req.file.originalname);
            res.redirect("/" + link);
        })
        .on('error', function(err) {
            res.send(err);
        });
}

// post-requests

exports.uploadAvatar = async function (req, res) {
    upload("./public/uploads/images/", "../uploads/images/", model.insertImage, req.cookies["login"], "edit", req, res);
};

exports.getUserApps = async function (req, res) {
    res.send( await model.getUserApps(req.body["nickname"]) );
};

exports.getAppsByName = async function (req, res) {
    res.send( await makeData( await model.getAppsByName(req.body["name"]) ) );
};