const model = require("../models/model");

// get-requests

async function makeData (apps) {
    let data = [];

    for (let app of apps) {
        let developer = await model.getUserById(app["developerID"]);
        data.push({ appId: app["_id"], appName: app["appName"], developer: developer["nickname"], likes: app["likes"],
            imgSrc: app["imgSrc"], downloadLink: app["downloadLink"]});
    }

    return data;
}

exports.edit = async function (req, res) {
    let currentUser = await model.getUser(req.cookies["login"]);

    res.render("edit.hbs", {
        login: currentUser["login"],
        fullName: currentUser["fullName"],
        nickname: currentUser["nickname"],
        email: currentUser["email"],
        phone: currentUser["phone"],
        bio: currentUser["bio"],
        imgSrc: currentUser["imgSrc"]
    });
};

exports.exit = function (req, res) {
    res.cookie("login", undefined, { maxAge: 900000, httpOnly: true });
    res.render("menu.hbs", {});
};

exports.getAppsNames = async function (req, res) {
    let apps = await model.getAppsNames(req.cookies["login"]);
    let names = [];

    for (let app of apps) {
        names.push(app["appName"]);
    }

    res.send(names);
};

exports.getTopUsersList = async function (req, res) {
    let users = await model.getTopUsersList();
    let data = [];

    for (let user of users) {
        data.push({ nickname: user["nickname"], likes: user["likes"], applicationsAmount: user["applicationsAmount"],
            imgSrc: user["imgSrc"]});
    }

    res.send(data);
};

exports.getTopAppsList = async function (req, res) {
    res.send( await makeData( await model.getTopAppsList() ) );
};

exports.getLikedApps = async function (req, res) {
    res.send( await model.getLikedApps(req.cookies["login"]) )  ;
};

// post-requests

exports.like = async function (req, res) {
    res.send( await model.like(req.body["appId"], req.cookies["login"]) );
};

exports.download = async function (req, res) {
    await model.addDownloads(req.body["appId"]);
    await res.download("./public/images/Back.png");
};

