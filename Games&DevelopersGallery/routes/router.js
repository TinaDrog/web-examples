const express = require("express"),
    adminController = require("../controllers/adminController"),
    menuController = require("../controllers/menuController"),
    mainController = require("../controllers/mainController"),
    editController = require("../controllers/editController"),
    myAppsController = require("../controllers/myAppsController"),
    generalController = require("../controllers/generalController"),
    router = express.Router(),
    jsonParser = express.json();

// general

router.get("/", generalController.menu);
router.get("/main", generalController.main);
router.get("/myApps", generalController.myApps);

router.post("/main", jsonParser, generalController.main);
router.post("/insert", jsonParser, generalController.insert);
router.post("/updateInfo", jsonParser, generalController.updateInfo);

// admin
router.get("/getUsersData", adminController.getUsersData);
router.get("/getAppsData", adminController.getAppsData);

router.post("/updateUserData", jsonParser, adminController.updateUserData);
router.post("/updateAppData", jsonParser, adminController.updateAppData);
router.post("/deleteUserData", jsonParser, adminController.deleteUserData);
router.post("/deleteAppData", jsonParser, adminController.deleteAppData);

// edit
router.post("/uploadAvatar", editController.uploadAvatar);
router.post("/getUserApps", jsonParser, editController.getUserApps);
router.post("/getAppsByName", jsonParser, editController.getAppsByName);

// main
router.get("/edit", mainController.edit);
router.get("/exit", mainController.exit);
router.get("/getAppsNames", mainController.getAppsNames);
router.get("/getTopUsersList", mainController.getTopUsersList);
router.get("/getTopAppsList", mainController.getTopAppsList);
router.get("/getLikedApps", mainController.getLikedApps);

router.post("/like", jsonParser, mainController.like);
router.post("/download", jsonParser, mainController.download);

// menu
router.get("/admin", menuController.admin);

router.post("/checkLogin", jsonParser, menuController.checkLogin);
router.post("/checkPassword", jsonParser, menuController.checkPassword);
router.post("/userInfo", jsonParser, menuController.userInfo);

// myApps
router.post("/addApp", jsonParser, myAppsController.addApp);
router.post("/deleteApp", jsonParser, myAppsController.deleteApp);
router.post("/addAppInfo", jsonParser, myAppsController.addAppInfo);
router.post("/uploadAppImage", myAppsController.uploadAppImage);
router.post("/uploadAppFile", myAppsController.uploadAppFile);
router.post("/resetImage", jsonParser, myAppsController.resetImage);
router.post("/updateAppInfo", jsonParser, myAppsController.updateAppInfo);

//questionnaire

module.exports = router;