// global variables
let clientHeight = document.documentElement.clientHeight;
let clientWidth = document.documentElement.clientWidth;

let containerWidth = clientWidth * 0.6;
let containerHeight = containerWidth / 1.6;     //1.6 - відношення ширини рисунка до довжини

let textContainerHeight = containerWidth * 13.3 / 100;
let textContainerWidth = containerWidth * 30.7 / 100;

let svgNS, svg, svg2, circles = [], submitCircle;
let isSignIn = true, isGuest = false, clicked = false;

//functions
function circleAnimation () {
    for (let i = 0; i < circles.length; i++) {
        (function (i) {
            timerID2 = setTimeout(function () {
                circles.forEach(circle => circle.style.opacity = "0");
                circles[i].style.opacity = "1";
            }, 700 * i);
        }(i));
    }
}

function setSizeAndPosition (id, params) {
    id = "#" + id;
    let properties = ["height", "width", "top", "left"];

    for (let i = 0; i < properties.length; i++) {
        $(id).css(properties[i], Math.round(params[i]) + "px");
    }
}

function createCircle (cxPercent, cyPercent, radius, color, width, id) {
    let circle = document.createElementNS(svgNS, "circle");

    let attrs = [
        ["cx", clientWidth * cxPercent / 100],
        ["cy", clientWidth * cyPercent / 100],
        ["r", radius], ["fill", color], ["fill-opacity", .1],
        ["stroke", color], ["stroke-width", width], ["id", id]];

    for (let i = 0; i < attrs.length; i++) {
        circle.setAttributeNS(null, attrs[i][0], attrs[i][1]);
    }

    return circle;
}

function generalEvent (element, hoverText, clickText) {
    $("#tip")
        .css("visibility", "visible")
        .html("...Press the button...");

    element.addEventListener("mouseover", async function() {
        await clearInterval(timerID);
        await clearTimeout(timerID2);

        clicked = false;

        submitCircle.classList.remove("invisibleButton");
        circles.forEach(circle => circle.style.opacity = "0");
        element.style.opacity = "1";

        $("#hoverText").css("display", "inline-block");
        $("#clickText, #clickText2")
            .css("display", "none")
            .html("");
        $("#login")
            .css("display", "none")
            .blur()
            .val("");
        $("#password")
            .css("display", "none")
            .blur()
            .val("");
        $("#submitCircle").css("display", "none");
        $("#tip").css("visibility", "hidden");

        drawText($("#hoverText"), hoverText);
    });

    element.addEventListener("mouseout", function() {
        if (!clicked) {
            element.style.opacity = "0";

            $("#hoverText")
                .html("")
                .css("display", "none");
            $("#tip")
                .css("visibility", "visible")
                .html("...Press the button...");
        }
    });

    element.addEventListener("click", async function() {
        $("#hoverText, #clickText, #clickText2").html("");
        $("#hoverText").css("display", "none");
        $("#clickText").css("display", "inline-block");

        circles.forEach(circle => circle.style.opacity = "0");
        element.style.opacity = "1";
        clicked = true;

        drawText($("#clickText"), clickText);
    });
}

function signEvent (element, hoverText, clickText){
    generalEvent(element, hoverText, clickText);

    element.addEventListener("mouseout", function() {
        if (!clicked)  {
            $("#login")
                .css("display", "none")
                .blur();
            $("#password").css("display", "none");
        }
    });

    element.addEventListener("click", async function() {
        isSignIn = hoverText === "Sign In";

        $("#login")
            .val("")
            .css("display", "inline");
        $("#password").val("");
        $("#tip")
            .css("visibility", "visible")
            .html("...Enter your login...");

        setTimeout(() => $("#login").focus(), 50 * clickText.length);

        isGuest = false;
    });
}

function skipEvent (element, hoverText, clickText) {
    generalEvent(element, hoverText, clickText);

    element.addEventListener("click", async function() {
        $("#tip")
            .css("visibility", "visible")
            .html("...Press the button to continue...");

        $("#submitCircle").css("display", "inline");
        submitCircle.classList.add("invisibleButton");

        isGuest = true;
    });
}

function forgetEvent (element, hoverText, clickText) {
    generalEvent(element, hoverText, clickText);

    element.addEventListener("click", async function() {
        isGuest = false;
    });
}

function setupSvg () {
    svgNS = "http://www.w3.org/2000/svg";

    svg = document.createElementNS(svgNS, "svg");
    svg.setAttribute("id", "svg");
    svg.setAttribute("height", containerWidth * 23.7 / 100 + "px");
    $("#svgContainer").append(svg);

    svg2 = document.createElementNS(svgNS, "svg");
    svg2.setAttribute("id", "svg2");
    svg2.setAttribute("height", containerWidth * 5.7 / 100 + "px");
    svg2.setAttribute("width", containerWidth * 5.7 / 100 + "px");
    $("#container").append(svg2);

    submitCircle = createCircle(1.7, 1.7, containerWidth * 2.7 / 100, "deepskyblue", 2, "submitCircle");

    $("#svg2")
        .css("top", containerWidth * 30 / 100)
        .css("left", containerWidth * 47.41 / 100)
        .append(submitCircle);
}

function setupCircles () {
    let radius = clientWidth * 2.02 / 100;
    let strokeWidth = clientWidth * 0.175 / 100;

    let signIn = createCircle(7.16, 2.8, radius, "#4bbed7", strokeWidth, "signIn");
    signEvent(signIn, "Sign In", "Login: ");
    svg.appendChild(signIn);

    let signUp = createCircle(11.59, 7.23, radius, "#f0888a", strokeWidth, "signUp");
    signEvent(signUp, "Sign Up", "Login: ");
    svg.appendChild(signUp);

    let skip = createCircle(7.2, 11.65, radius, "#97b9d7", strokeWidth, "skip");
    skipEvent(skip, "Skip", "You will be a guest and won't have additional functionality");
    svg.appendChild(skip);

    let forget = createCircle(2.78, 7.23, radius, "#cfa7ce", strokeWidth, "lang");
    forgetEvent(forget, "Forget password", "Please, ask Administrator for help!");
    svg.appendChild(forget);

    circles = [signIn, signUp, skip, forget];
}

function setupElements() {
    $("#hoverText")
        .css("width", textContainerWidth + "px")
        .css("line-height", textContainerHeight + "px")
        .css("font-size", textContainerHeight * 0.3 + "px");

    $("#clickText")
        .css("padding", textContainerHeight * 0.2 + "px" + " 0 0 10px");

    $("#clickText, #clickText2, #login, #password")
        .css("font-size", textContainerHeight * 0.2 + "px");

    $("#login")
        .css("padding-top", textContainerHeight * 0.2 + "px")
        .css("width", textContainerWidth - $("#clickText2").length - parseInt($("#login").css("width")) / 3 + "px")
        .on("keypress", function(event) {
            $("#tip").html("...Press Enter...");
            if (event.key === "Enter") {
                checkLogin($("#login").val());
            }
        });

    $("#password")
        .css("padding-top", textContainerHeight * 0.1 + "px")
        .css("margin-top", 0)
        .css("width", textContainerWidth - $("#clickText2").length - 10 + "px");

    $("#tip")
        .css("width", textContainerWidth + "px")
        .css("font-size", textContainerHeight * 0.15 + "px");

    $("#submitCircle")
        .on("click", function () {
            if (!isSignIn && !isGuest) {
                insertUser();
                $("#singForm").attr("action", "/userInfo");
            }

            $("#singForm").submit();
        });
}

function drawText (element, fullText) {
    let current = "";

    for (let i = 0; i < fullText.length; i++) {
        (function (i) {
            setTimeout(function() {
                current += fullText[i];
                element.html(current);
            }, 50 * i);
        }(i));
    }
}

function passwordSettings () {
    let pass = undefined;

    $("#login").blur();

    $("#clickText2").css("display", "inline-block");

    $("#tip").html(isSignIn ? "...Enter your password..." : "...Password: at least 8 symbols (0-9 A-Z - _)...");

    $("#password")
        .css("display", "inline")
        .css("width", textContainerWidth / 1.7 + "px")
        .on("keypress", function (event) {
            $("#tip").html("...Press Enter...");

            if (event.key === "Enter") {
                if (pass !== undefined) {
                    if (pass === $("#password").val()) {
                        circles.forEach(circle => circle.style.display = "none");
                        submitCircle.classList.add("invisibleButton");

                        $("#tip").html("...Press the button to continue...");
                        $("#submitCircle").css("display", "inline");

                        return;
                    } else {
                        $("#password").val("");
                        $("#tip").html("...Incorrect retyped password...");
                    }
                }

                if (isSignIn) {
                    checkPassword($("#login").val(), $("#password").val());
                } else {
                    let result = /^[a-z\d_\-]{8,}$/i.test($("#password").val());
                    pass = undefined;

                    if (result) {
                        pass = $("#password").val();

                        $("#password").val("");
                        $("#tip").html("...Retype password...");
                    } else {
                        $("#tip").html("...Incorrect password!...");
                        $("#password").val("");
                    }
                }
            }
        });

    drawText($("#clickText2"), "Password: ");
    setTimeout(() => $("#password").focus(), 50 * 10);
}

function checkLogin (login) {
    $.ajax({
        url: "/checkLogin",
        contentType: "application/json",
        type: "POST",
        data: JSON.stringify({
            login: login
        }),
        success: function (result) {
            if (result === "") {
                if (isSignIn) {
                    $("#login").val("");
                    $("#tip").html("...This login doesn't exist!...");
                } else {
                    $("#tip").html("...Success...");
                    passwordSettings();
                }
            } else {
                if (isSignIn) {
                    passwordSettings();
                } else {
                    $("#login").val("");
                    $("#tip").html("...This login is already exists!...");
                }
            }
        }
    });
}

function checkPassword (login, password) {
    $.ajax({
        url: "/checkPassword",
        contentType: "application/json",
        type: "POST",
        data: JSON.stringify({
            login: login,
            password: password
        }),
        success: function (result) {
            if (result === "") {
                $("#password").val("");
                $("#tip").html("...Wrong password!...");
            } else {
                circles.forEach(circle => circle.style.display = "none");
                $("#tip").html("...Press the button to continue...");
                $("#submitCircle").css("display", "inline");
                submitCircle.classList.add("invisibleButton");
            }
        }
    });
}

function insertUser () {
    $.ajax({
        url: "/insert",
        contentType: "application/json",
        type: "POST",
        data: JSON.stringify({
            login: $("#login").val(),
            password: $("#password").val()
        }),
        error: function () {
            $("#login, #password").val("");
            $("#login").focus();
            $("#clickText2").html("");
            $("#tip").html("...Smth went wrong...");
        }
    })
}

//initial settings
setSizeAndPosition("container", [
    "inherit",
    containerWidth,
    (clientHeight - containerHeight) / 2,
    (clientWidth - containerWidth) / 2
]);
setSizeAndPosition("svgContainer", [
    containerWidth * 23.7 / 100,
    containerWidth * 23.7 / 100,
    containerHeight * 10.6 / 100,
    containerWidth * 69.5 / 100
]);
setSizeAndPosition("textContainer", [
    textContainerHeight,
    textContainerWidth,
    containerWidth * 7.4 / 100,
    containerWidth * 34.8 / 100
]);
setSizeAndPosition("submit", [
    containerWidth * 5.4 / 100,
    containerWidth * 5.4 / 100,
    containerWidth * 22.8 / 100,
    containerWidth * 12.7 / 100
]);

setupSvg();
setupCircles();
setupElements();

let timerID, timerID2;
circleAnimation();
timerID = setInterval(circleAnimation, 800 * 4);