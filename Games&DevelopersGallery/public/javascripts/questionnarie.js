let max = getMaxLength();
let elements = [
    [ $("#fullNameLabel"), $("#fullName"), /^[A-Za-z' \-]+$/, "...Full name may include only letters, apostrophe and gap..."],
    [ $("#nicknameLabel"), $("#nickname"), /[^ ]{5,}/, "...Nickname should include at least 5 characters..." ],
    [ $("#emailLabel"), $("#email"), /^[^@\s]+@[^@\s]+\.[a-z]+$/, "...Write the correct email..." ],
    [ $("#phoneLabel"), $("#phone"), /(\+\d{3})?\d{7,}/, "...Phone number may begin from + => code => number" ],
    [ $("#bioLabel"), $("#bio"), /.+/, "" ]
];
let pointer = 0;

function getMaxLength () {
    let max = 0, length = 0;
    let labels = document.getElementsByTagName("label");
    let amount = labels.length;

    for (let i = 0; i < amount; i++) {
        length = labels[i].innerHTML.length;

        if (length > max) {
            max = length;
        }
    }

    return max;
}

let clientWidth = document.documentElement.clientWidth;
let clientHeight = document.documentElement.clientHeight;

let fontSize = clientWidth * 0.04 ;
let labelHeight = fontSize * 1.04;
let labelWidth = fontSize * max / 2.5;
let labelTop = (clientHeight - labelHeight) / 3;
let labelLeft = (clientWidth - labelWidth) / 3;

function initial () {
    $("label, input, body").css("font-size", fontSize);
    $("label")
        .css("width", labelWidth)
        .css("top", -(labelTop))
        .css("left", labelLeft);
    $("input")
        .css("bottom", 0 + "px")
        .css("left", labelLeft + labelWidth / 3)
        .on("input", () => $("#tip").css("opacity", 0))
        .on("keypress", function (event) {
            if (event.key === "Enter") {
                event.preventDefault();

                document.removeEventListener("click", clickEvent, false);

                if (!elements[pointer][2].test(elements[pointer][1].val())) {
                    tipAnimation(elements[pointer][3]);
                    return;
                }

                pointer !== elements.length - 1
                    ? $("#next").click()
                    : $("#submit").click();

                document.addEventListener("click", clickEvent, false);
            }
        });
    $("#tip")
        .css("font-size", fontSize / 2.5)
        .css("font-style", "italic")
        .css("bottom", clientHeight - (labelTop + 4 * labelHeight))
        .css("left", parseInt($("input").css("left"))
            + (parseInt($("input").css("width")) - parseInt($("#tip").css("width"))) / 2);
    $("#submit").on("click", updateInfo);

    setTimeout( function () {
        $("#submit")
            .css("font-size", fontSize / 1.5)
            .css("bottom", parseInt($("#previous").css("bottom")) - parseInt($("#submit").css("height")) * 2)
            .css("left",(clientWidth - parseInt($("#submit").css("width"))) / 2);
    }, 1000);

}

function updateInfo () {
    $.ajax({
        url: "/updateInfo",
        contentType: "application/json",
        type: "POST",
        data: JSON.stringify({
            login: $("#login").html(),
            fullName: $("#fullName").val(),
            nickname: $("#nickname").val(),
            email: $("#email").val(),
            phone: $("#phone").val(),
            bio: $("#bio").val()
        })
    });
}

function setupButtons () {
    let increasingDurDiff = 0.2, textFadingDurDiff = 0.25;
    let width = clientWidth * 3.58 / 100,
        nextBottom = clientHeight - (labelTop + 2.8 * labelHeight),
        skipBottom = nextBottom + width * 1.6,
        previousBottom = nextBottom - width * 1.6,
        fontSize = clientWidth * 1.56 / 100;
    let buttons = [ [ $("#skip"), skipBottom ], [ $("#next"), nextBottom ], [ $("#previous"), previousBottom ] ];

    $("span").css("font-size", fontSize);

    for (let i = 0; i < buttons.length; i++) {
        let left = clientWidth * (i !== 1 ? 72.92 : 75.52) / 100;

        buttons[i][0]
            .css("bottom", buttons[i][1])
            .css("width", clientWidth * 3.58 / 100)
            .css("left", left)
            .css("animation", `.5s ${1.2 + increasingDurDiff * i}s 1 ease-out forwards increasing`)
            .next()
                .css("bottom", buttons[i][1] + width / 3.5)
                .css("left", left + width * 1.5)
                .css("animation", `1s ${1.3 + textFadingDurDiff * i}s 1 ease-out forwards textFading`);
    }
}

function setupEvents() {
    function show() {
        $("#tip").css("opacity", 0);
        hide(elements[pointer][0], elements[pointer][1]);
        showNext(elements[++pointer][0], elements[pointer][1]);
        if (pointer === elements.length - 1) {
            setTimeout( function () {
                $("#submit").css("display", "block").css("animation", "2s infinite alternate wave");
            }, 1500);
        }
    }

    $("#skip").on("click", function () {
        $("#tip").css("opacity", 0);

        if (pointer === elements.length - 1) return;

        elements[pointer][1].val("");
        show();
    });
    $("#next").on("click", function () {
        if (pointer === elements.length - 1) return;
        if (elements[pointer][1].val() === "") {
            tipAnimation("...Enter a value or press skip...");
            return;
        }

        show();
    });
    $("#previous").on("click", function () {
        $("#tip").css("opacity", 0);

        if (pointer === 0) return;

        hide(elements[pointer][0], elements[pointer][1], false);
        showPrevious(elements[--pointer][0], elements[pointer][1]);
    });
}

function showNext (label, input) {
    label
        .animate({ opacity: .3, top: labelTop + labelHeight / 2 }, { duration: 1200 })
        .animate({ opacity: 1 }, 400);
    input
        .animate({ opacity: .3, bottom: clientHeight - (labelTop + 2.8 * labelHeight) },{ duration: 1200 })
        .animate({ opacity: 1 }, 400)
        .focus();
}

function showPrevious (label, input) {
    label
        .animate({ opacity: .3, left: labelWidth }, { duration: 1200 })
        .animate({ opacity: 1 }, 400);
    input
        .animate({ opacity: .3, left: labelLeft + labelWidth / 3}, { duration: 1200 })
        .animate({ opacity: 1 }, 400)
        .blur();
    setTimeout( () => {input.focus()}, 1500);
}

function hide (label, input, isHorizontal = true) {
    let labelOption = isHorizontal ? { opacity: 0, left: 0 - labelWidth } : { opacity: 0, top: -(labelTop) };
    let inputOption = isHorizontal ? { opacity: 0, left: clientWidth } : { opacity: 0, bottom: 0 };
    label
        .animate(labelOption,{ duration: 1200 });
    input
        .blur()
        .animate(inputOption, { duration: 1200 });
}

function tipAnimation (html) {
    $("#tip")
        .html(html)
        .animate({opacity: 1}, 1500)
        .css("opacity", 1)
        .css("left", parseInt(elements[pointer][1].css("left"))
            + (parseInt(elements[pointer][1].css("width")) - parseInt($("#tip").css("width"))) / 2)
        .css("animation", "1.5s 1.5s changeAlpha infinite");
}

initial();
setupButtons();
setupEvents();
showNext(elements[pointer][0], elements[pointer][1]);

/*animation
            .on("mouseover", function () {
                buttons[i][0].next()
                    .animate({bottom: buttons[i][1] + width / 3.5 + 8}, 400)
                    .animate({bottom: buttons[i][1] + width / 3.5}, 400)
                buttons[i][0].next()
                    .animate({bottom: buttons[i][1] + width / 3.5 - 8 }, 800);

                timerId = setInterval( () => {
                     buttons[i][0].next()
                         .animate({bottom: buttons[i][1] + width / 3.5 + 8}, 400)
                         .animate({bottom: buttons[i][1] + width / 3.5}, 400)
                }, 1000);
            })
            .on("mouseout", function () {
                clearInterval(timerId);
                buttons[i][0].next()
                    .animate({bottom: buttons[i][1] + width / 3.5 }, 800);
            })*/

