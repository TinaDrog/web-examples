let clientWidth = document.documentElement.clientWidth;
let clientHeight = document.documentElement.clientHeight;

let photoWidth = clientWidth * 16.28 / 100;
let saveWidth = parseInt($("#save").css("width"));

$("body").css("height", clientHeight);
$("input").css("width", clientWidth * 0.3);

$("#changePhoto")
    .on("click", async function () {
        let file = $("#file").click();
        let id = setInterval(function () {
            if (file.val()) {
                clearInterval(id);
                $("#upload").click();
            }
        }, 500);

    })
    .css("bottom", photoWidth / 2);
$("#photo")
    .css("width", photoWidth)
    .css("height", photoWidth);
$("a").css("bottom", clientHeight * 0.1);
$("#result").css("bottom", clientHeight * 0.15).css("right", clientWidth * 0.245);
$("#cancel, #back").css("width", saveWidth);
$("#save").css("right", clientWidth * 0.25).on("click", function () {
    updateInfo();
    $("#result").css("display", "inline").css("animation", "3s changeOpacity");
    setTimeout( () => {
        $("#result").css("display", "none").css("animation", "");
    }, 3000);
});
$("#cancel").css("right", clientWidth * 0.25 + saveWidth * 1.3);
$("#back").css("right", clientWidth * 0.25 - saveWidth * 1.3);

function updateInfo () {
    $.ajax({
        url: "/updateInfo",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({
            login: $("#login").val(),
            fullName: $("#fullName").val(),
            nickname: $("#nickname").val(),
            email: $("#email").val(),
            phone: $("#phone").val(),
            bio: $("#bio").val()
        })
    });
}