let selectedTr, isUsers;
let usersData = [], appsData = [];

let userHeaders = ["id", "login", "full name", "nickname", "email", "phone", "bio", "likes", "apps", "image", "action"];
let userKeys = ["_id", "login", "fullName", "nickname", "email", "phone", "bio", "likes", "applicationsAmount", "imgSrc"];
let appHeaders = ["id", "app name", "description", "developer id", "download link", "downloads", "image", "likes", "action"];
let appKeys = ["_id", "appName", "description", "developerID", "downloadLink", "downloads", "imgSrc", "likes"];

function getData () {
    $.ajax({
        url: isUsers ? "/getUsersData" : "/getAppsData",
        contentType: "application/json",
        type: "get",
        success: function (data) {
            if (isUsers) {
                usersData = data;
                renderTable(usersData, userHeaders, userKeys);
            } else {
                appsData = data;
                renderTable(appsData, appHeaders, appKeys);
            }

        }
    });
}

function renderTable (data, headers, keys) {
    let table = `<table><thead><tr>`;

    for (let header of headers) {
        table += `<td>${header}</td>`
    }

    table += `</tr></thead>`;

    let i = 0;

    for (let d of data) {
        table += `<tr id="${i}">`;

        for (let key of keys) {
            table += `<td>${d[key]}</td>`;
        }

        table += `<td id="images">
                      <img src="./images/edit.png" id="edit"/>
                      <img src="./images/trash.png" id="delete"/>
                  </td></tr>`;

        i++;
    }

    table += "</table>";

    $("#info").append(table);
}

function update (index) {
    let data = isUsers ? usersData[index] : appsData[index];

    $.ajax({
        url: isUsers ? "/updateUserData" : "/updateAppData",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({ data: data })
    });
}

function del (id) {
    $.ajax({
        url: isUsers ? "/deleteUserData" : "/deleteAppData",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({ id: id })
    });
}

function setupEvents () {
    function resetTicks () {
        $("tr").attr("contenteditable", false).css("background-color", "transparent");
        $("#edit.tick").attr("src", "./images/edit.png").attr("class", "");
        $("#delete.tick").attr("src", "./images/trash.png").attr("class", "");
    }
    setTimeout(() => {
        $("td > #edit").on("click", function (event) {
            if ($(this).attr("class") === "tick") {
                resetTicks();

                let index = $(selectedTr).attr("id");
                let tdData = $(selectedTr).find("> td");
                let arrays = isUsers
                    ? { keys: userKeys, data: usersData, headers: userHeaders }
                    : { keys: appKeys, data: appsData, headers: appHeaders };

                for (let i = 0; i < arrays["keys"].length; i++) {
                    arrays["data"][index][arrays["keys"][i]] = tdData[i].innerHTML;
                }

                update($(selectedTr).attr("id"));
                $("table").remove();
                renderTable(arrays["data"], arrays["headers"], arrays["keys"]);
                setupEvents();

                return;
            }

            resetTicks();

            selectedTr = event.target.parentElement.parentElement;
            $(selectedTr).attr("contenteditable", true).css("background-color", "#16ac1a5e");
            $(this).attr("src","./images/tick.png").attr("class", "tick");
        });
        $("td > #delete").on("click", function (event) {
            if ($(this).attr("class") === "tick") {
                resetTicks();

                let index = $(selectedTr).attr("id");
                let arrays = isUsers
                    ? { keys: userKeys, data: usersData, headers: userHeaders }
                    : { keys: appKeys, data: appsData, headers: appHeaders };
                let id = arrays["data"][index]["_id"];

                arrays["data"].splice(index, 1);
                del(id);
                $("table").remove();
                renderTable(arrays["data"], arrays["headers"], arrays["keys"]);

                return;
            }

            resetTicks();

            selectedTr = event.target.parentElement.parentElement;
            $(selectedTr).attr("contenteditable", true).css("background-color", "#ac161a5e");
            $(this).attr("src","./images/tick.png").attr("class", "tick");
        });
    },500);
}

function setupPage () {
    $("table").remove();
    getData();
    setupEvents();
}

$("#users").on("click", async function () {
    isUsers = true;

    $(this).attr("class", "selected").next().attr("class", "");
    await setupPage();
}).click();
$("#apps").on("click", async function () {
    isUsers = false;

    $(this).attr("class", "selected").prev().attr("class", "");
    await setupPage();
});


