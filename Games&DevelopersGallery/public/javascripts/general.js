function clickEvent (event) {
    let clickDiv = document.createElement("div");
    clickDiv.setAttribute("class", "clickEffect");
    clickDiv.style.top = event.clientY + "px";
    clickDiv.style.left = event.clientX + "px";
    $("body").append(clickDiv);
    clickDiv.addEventListener('animationend', function () {
        clickDiv.parentElement.removeChild(clickDiv);
    });
}
document.addEventListener('click', clickEvent);

let forbiddenSymbols = "йцукенгшщзхїфівапролджєячсмитьбюыэъёЫЭЪЁЙЦУКЕНГШЩЗХЇФІВАПРОЛДЖЄЯЧСМИТЬБЮ";
$("body").on("keypress", function (event) {
    if (forbiddenSymbols.includes(event.key)) {
        alert("Sorry, but only English letters are allowed!");
        event.preventDefault();
    }
});
