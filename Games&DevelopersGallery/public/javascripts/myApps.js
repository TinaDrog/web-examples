let clientWidth = document.documentElement.clientWidth;
let clientHeight = document.documentElement.clientHeight;

let currentApp;

function cssSettings () {
    $("body")
        .css("font-size", clientWidth * 0.02);
    $("button")
        .on("click", function () {
            $("button").css("outline", "none");
        });
    $("textarea")
        .css("height", clientWidth / 12);
    $("input, textarea")
        .css("font-size", clientWidth * 0.015)
        .css("width", clientWidth * 0.3);
    $("label, #appDetails a").css("font-size", clientWidth * 0.015);

    $("#container")
        .css("width", clientWidth * 0.97)
        .css("height", clientHeight);
    $("#appPicture")
        .css("width", clientWidth * 0.35);
    $("#addApp").on("click", addApp);
    $("#changePhoto, #changeFile").on("click", function (event) {
        let parent = event.target.parentElement;
        upload($(parent).attr("id"));
    });
    $("#deletePhoto").on("click", function () {
        $.ajax({
            url: "/resetImage",
            contentType: "application/json",
            type: "post",
            data: JSON.stringify({
                appName: $("#appName").val()
            })
        });
        location.reload();
    });
    $("#save").on("click", updateInfo);
    $("#delete").on("click", function () {
        let doDelete = confirm("Your app will be deleted from this website. Are you sure that you want to delete it?");

        if (doDelete) {
            deleteApp();
        }
    });
    $("#uploadFile").css("width", clientWidth * 0.08);
}

function upload (parent) {
    let file = $(`#${parent} > #file`).click();
    let id = setInterval(function () {
        if (file.val()) {
            $("#link").val(file.val());
            clearInterval(id);
            $(`#${parent} > #upload`).click();
        }
    }, 500);
}

function getAppsNames () {
    $.ajax({
        url: "/getAppsNames",
        contentType: "application/json",
        type: "get",
        success: function (names) {
            if (names.length === 0) {
                $("#appPhoto, #details").css("display", "none");
                $("#appDetails").append("<p id='tip'>You don't have any application. Click '+' to add one.</p>");
                $("#tip").css("font-size", clientWidth * 0.03);
            }
            for (let name of names) {
                $("#appPhoto, #details").css("display", "block");
                $("#tip").remove();

                let li = $("<li></li>").html(name).attr("id", name).on("click", liClick);
                $("#apps").prepend(li);
            }
        }
    });
}

function liClick() {
    $.ajax({
        url: "/addAppInfo",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({
            appName: $(this).html()
        }),
        success: function (result) {
            let img = new Image();
            img.src = result["imgSrc"];

            let width = img.height > img.width ? "" : clientWidth * 0.3;
            let height = img.height > img.width ? clientWidth * 0.2 : "";

            $("#name, #appName").val(result["appName"]);
            $("#description").val(result["description"]);
            $("#link").val(result["downloadLink"]);
            $("#likes").val(result["likes"].length);
            $("#downloads").val(result["downloads"]);
            $("#appPicture > img")
                .attr("src", result["imgSrc"])
                .css("width", width)
                .css("height", height);

            currentApp = result["appName"];
            if (result["likes"] >= 500) {
                $("#delete").css("opacity", .5).attr("disabled", "disabled");
            } else {
                $("#delete").css("opacity", 1).attr("disabled", "enabled");
            }
        }
    });

    $("li").attr("class", "");
    $(this).attr("class", "selected");
    $("#delete, #back").css("visibility", "visible");
    $("#add").html("Save changes").attr("id", "save");
}

function addApp () {
    $("#appPhoto, #details").css("display", "block");
    $("#tip").remove();

    let liName = "App" + ($("li").length + 1);

    $("li").attr("class", "");
    $("#name, #appName").val(liName);
    $("#description").val("");
    $("#link").val("");
    $("#likes").val("");
    $("#downloads").val("");
    $("#appPicture > img").attr("src", "./images/appDefault.jpg");
    $("#delete, #back").css("visibility", "hidden");
    $("#save").html("Add a new app").attr("id", "add");

    let li = $("<li></li>").html(liName).attr("id", liName).on("click", liClick).attr("class", "selected");
    $("#apps").prepend(li);

    $.ajax({
        url: "/addApp",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({
            appName: liName
        })
    });
    setTimeout( () => { $("li").first().click() }, 100);
}

function updateInfo () {
    $.ajax({
        url: "/updateAppInfo",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({
            currentApp: currentApp,
            appName: $("#name").val(),
            description: $("#description").val(),
            downloadLink: $("#link").val()
        })
    });
    $("#result")
        .html("...Changes saved")
        .css("display", "inline-block")
        .css("animation", "5s changeOpacity");
    setTimeout( () => {
        $("#result").css("display", "none").css("animation", "");
    }, 5000);
}

function deleteApp () {
    $.ajax({
        url: "/deleteApp",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({
            appName: currentApp
        }),
        success: function (result) {
            $("#result")
                .html(result ? "...Successful deleted" : "...You can't delete app that has more than 500 likes")
                .css("display", "inline-block")
                .css("animation", "5s changeOpacity");
            setTimeout( () => {
                $("#result").css("display", "none").css("animation", "");
            }, 5000);

            if (result) {
                $(".selected").remove();
                setTimeout( () => {$("li").first().click()}, 100);
            }
        }
    });
}

cssSettings();

getAppsNames();
setTimeout( () => {$("li").first().click()}, 100);
