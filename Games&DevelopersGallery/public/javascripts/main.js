let clientWidth = document.documentElement.clientWidth;
let clientHeight = document.documentElement.clientHeight;

let headerHeight = clientHeight * 0.07;
let buttons = [ [ $("#myApps"), $("#edit"), $("#exit") ], [$("#topUsers"), $("#topApps"), $("#noFilters")] ];
let avClickAmount = 0, filClickAmount = 0;

function scale (buttons, isAvatar = true) {
    if ((isAvatar ? avClickAmount : filClickAmount) % 2 === 0) {
        let delay = .1;
        for (let i = 0; i < buttons.length; i++) {
            buttons[i].css("display", "block").css("animation", `.2s ${delay * i}s forwards scaleToOne`);
        }
    } else {
        buttons.forEach((button) => button.css("display", "none"));
    }

    isAvatar ? avClickAmount++ : filClickAmount++;
}

function setupHeader () {
    $("button, input")
        .on("click", () => $("button, input").css("outline", "none"))
        .click();
    $("#header").css("height", headerHeight);
    $("#nickname").css("font-size", headerHeight / 3);
    $("#userAvatar")
        .css("height", headerHeight)
        .css("width", headerHeight)
        .on("click", function () {
            scale(buttons[0]);
        });
    $("#search, #filter").css("height", headerHeight / 1.5);
    $("#search")
        .css("width", clientWidth / 4)
        .css("font-size", headerHeight / 3)
        .on("input", function (event) {
            let str = $(this).val();

            if (str.match(/^[A-Za-z\d-_']+$/)) {
                getAppsByName(str);
            } else {
                $("#apps > *").remove();
                getAppsList();
            }
        });
    $("#filter")
        .on("click", function () {
            $("#filters > a").css("display", "inline");
            setTimeout(() => scale(buttons[1], false), 100);
        })
        .css("font-size", clientWidth * 0.02)
        .css("width", clientWidth * 0.075);

    let avatarOffset = $("#userAvatar").offset(),
        appsWidth = parseInt($("#myApps").css("width")),
        filterOffset = $("#filter").offset();

    $("#edit, #exit").css("width", appsWidth);
    $("#topApps, #topUsers, #noFilters").css("width", clientWidth * 4.45 / 100)
        .on("click", function (event) {
            $("#filter").html(event.target.innerHTML);
        });
    $("#noFilters, #topApps").on("click", function () {
        $("#users > *, #apps > *").remove();
        initial();
    });
    $("#topUsers").on("click", function () {
        $("#users > *, #apps > *").remove();
        initial();
        $("#users > *").attr("class", "");
        setTimeout(() => $("#users > *").first().attr("class", "selectedUser"), 50);
    });

    let left_1 = avatarOffset.left - appsWidth * 3, left_2 = filterOffset.left - clientWidth * 0.085;
    for (let i = 0; i < buttons[0].length; i++) {
        buttons[0][i].css("top", headerHeight + 20).css("left", left_1);
        buttons[1][i].css("top", headerHeight + 20).css("left", left_2);

        left_1 += appsWidth * 3;
        left_2 += clientWidth * 0.085;
    }
    $("#users #info").css("font-size", clientWidth * 0.04);
}

function renderUsers (users) {
    for (let i = 0; i < users.length; i++) {
        let appAmount = users[i]["applicationsAmount"];
        let likeAmount = users[i]["likes"];

        let user = `<div id=${users[i]["nickname"]}>
                                <div id="photo" style="background: #1e1e1e url('${users[i]["imgSrc"]}') center; background-size: cover;"></div>
                                <div id="info">
                                    <p id="name">${users[i]["nickname"]}</p>
                                    <p id="appsAmount">${appAmount} ${appAmount === 1 ? "app" : "apps"}</p>
                                    <p id="likesAmount">${likeAmount} ${likeAmount === 1 ? "like" : "likes"}</p>
                                 </div>
                             </div>`;

        $("#users").append(user);
        $("#users #info").css("font-size", clientWidth * 0.015);
    }
}

function getUsersList() {
    $.ajax({
        url: "/getTopUsersList",
        contentType: "application/json",
        type: "get",
        success: function (result) {
            renderUsers(result);
        }
    });
}

function renderApps (apps, nickname = "") {
    for (let i = 0; i < apps.length; i++) {
        let likeAmount = apps[i]["likes"].length;
        let developer = nickname === "" ? apps[i]["developer"] : nickname;

        let app = `<div id=${apps[i]["appName"]}>
                        <div id="picture" style="background: url('${ apps[i]["imgSrc"] }') center no-repeat; background-size: contain;"></div>
                        <div id="info">
                            <p id="name">Name: ${apps[i]["appName"]}</p>
                            <p id="developer">Developer: ${developer}</p>
                            <p id="likes">${likeAmount} ${likeAmount === 1 ? "like" : "likes"}</p>
                        </div>
                        <a id="like" class="" >Like</a>
                        <p hidden="hidden" id="appId">${apps[i]["appId"]}</p>
                        <a id="download" href="${apps[i]["downloadLink"]}" download>Download</a>
                   </div>`;

        $("#mainContent > #apps").append(app);
        $("#like, #download").css("font-size", parseInt($("#download").css("width")) * 0.3);
    }
}

function getAppsList () {
    $.ajax({
        url: "/getTopAppsList",
        contentType: "application/json",
        type: "get",
        success: function (result) {
            renderApps(result);
        }
    });
}

function getLikedApps() {
    $.ajax({
        url: "/getLikedApps",
        contentType: "application/json",
        type: "get",
        success: function (names) {
            for (let name of names) {
                $(`#${name} > #like`).attr("class", "liked");
            }
        }
    });
}

function setupMainContent () {
    $("a#like").on("click", function (event) {
        let app = event.target;
        let appId = $(app).next().html();
        likesToggle(appId, $(this));
    });
    $("#apps > div > #download").on("click", function (event) {
        let downloadButton = event.target;
        let appId = $(downloadButton).prev().html();
        download(appId);
    });
    $("#users > *").on("click", async function (event) {
        $("#users > *").attr("class", "");

        let element = event.target.parentElement;
        $(element).attr("class", "selectedUser");

        getUserApps($(element).attr("id"));
    })
}

async function getUserApps (nickname) {
    $.ajax({
        url: "/getUserApps",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({ nickname: nickname }),
        success: function (apps) {
            $("#apps > *").remove();
            renderApps(apps, nickname);
            setTimeout(getLikedApps, 50);
        }
    });
}

function initial () {
    getUsersList();
    getAppsList();

    setTimeout(getLikedApps, 50);
    setTimeout(setupMainContent, 100);
}

function likesToggle (appId, likeElement) {
    $.ajax({
        url: "/like",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({ appId: appId }),
        success: function (result) {
            let appLikesAmount = result["likeAmount"];
            let userLikesAmount = result["userLikes"];

            likeElement.prev().find("#likes").html(appLikesAmount + " " + (appLikesAmount === 1 ? "like" : "likes"));
            likeElement
                .attr("class", result["isLiked"] ? "liked" : "")
                .html(result["isLiked"] ? "Liked" : "Like");
            console.log($(`#${result["nickname"]} #likesAmount`));
            $(`#${result["nickname"]} #likesAmount`).html(userLikesAmount + " " + (userLikesAmount === 1 ? "like" : "likes"));
        }
    });
}

function download (appId) {
    $.ajax({
        url: "/download",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({ appId: appId })
    });
}

function getAppsByName (name) {
    $.ajax({
        url: "/getAppsByName",
        contentType: "application/json",
        type: "post",
        data: JSON.stringify({ name: name }),
        success: function (apps) {
            $("#apps > *").remove();
            renderApps(apps);
        }
    });
}

setupHeader();

initial();
