const express = require('express'),
    mongoose = require("mongoose"),
    bodyParser = require("body-parser"),
    router = require("./routes/router");
const app = express(),
    path = require("path");
const session = require("express-session"),
    cookieParser = require("cookie-parser"),
    multer = require('multer');

app.set("view engine", "hbs");
app.use(bodyParser.urlencoded({extended: false}));

app.use(multer({dest:"/public/uploads/images"}).single("file"));

app.use(express.static(path.join(__dirname, '/public/')));

const MongoStore = require("connect-mongo")(session);
app.use(session({
    secret: "keyboard cat",
    saveUninitialized: false,
    resave: false,
    store: new MongoStore({mongooseConnection: mongoose.connection})
}));
app.use(cookieParser());
app.use("/", router);

app.use(function (req, res, next) {
    res.status(404).send("<h1>This page was not found!</h1>");
});

app.listen(3000);
