const mongoose = require("mongoose"),
    bcrypt = require("bcrypt"),
    Schema = mongoose.Schema;

mongoose.connect("mongodb://localhost:27017/Rating", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const userSchema = new Schema({
    login: { type: String, required: true },
    password: { type: String, minlength: 8, required: true },
    fullName: { type: String, default: null },
    nickname: { type: String, default: null },
    email: { type: String, default: null },
    phone: { type: String, default: null },
    bio: { type: String, default: null },
    likes: { type: Number, default: 0 },
    applicationsAmount: { type: Number, default: 0 },
    imgSrc: { type: String, data: Buffer, default: "../images/avatarDefault.jpeg"}
}, { versionKey: false }, { typeKey: '$type' });
const appSchema = new Schema({
    appName: { type: String, required: true },
    developerID: { type: mongoose.ObjectId, required: true },
    description: { type: String, default: "" },
    likes: { type: Array },
    downloads: { type: Number, default: 0 },
    imgSrc: { type: String, data: Buffer, default: "./images/appDefault.jpg"},
    downloadLink: { type: String }
}, { versionKey: false }, { typeKey: '$type' });

const User = mongoose.model("User", userSchema, "Users");
const App = mongoose.model("App", appSchema, "Apps");

exports.addUser = async function (login, password) {
    try {
        const salt = await bcrypt.genSalt();
        const hashedPassword = await bcrypt.hash(password, salt);

        return User.create({login: login, password: hashedPassword}, function (error, result) {
            if (error) throw error;
        });
    } catch (e) {
        console.log(e);
    }
};

async function getUser(parameters) {
    let res = undefined;

    await User.findOne(parameters, function (err, result) {
        if (err) throw err;
        res = result;
    });

    return res;
}

exports.getUser = async function (login) {
    return await getUser({ login: login });
};

exports.getUserById = async function (id) {
    return await getUser({ _id: id });
};

exports.checkLogin = async function (login) {
    return await getUser({ login: login });
};

exports.checkPassword = async function (login, password) {
    let user = await getUser({ login: login });

    try {
        if (await bcrypt.compare(password, user['password'])) {
            return user;
        } else {
            return "";
        }
    } catch (e) {
        return "";
    }
};

exports.updateInfo = async function (login, fullName, nickname, email, phone, bio) {
    return await User.updateOne(
        { login: login },
        { $set: {fullName: fullName, nickname: nickname, email: email, phone: phone, bio: bio }},
        function (error, result) {
            if (error) console.log(error);
        }
    );
};

exports.insertImage = async function (login, imgSrc) {
    return await User.updateOne(
        { login: login },
        { $set: { imgSrc: imgSrc } },
        function (err, result) {
            if (err) return console.log(err);
        }
    );
};

exports.addApp = async function (login, appName, developerID, description, imgSrc, downloadLink) {
    let user = await getUser({login: login});
    let id = developerID === undefined ? user["_id"] : developerID;

    await User.updateOne(
        { login: login },
        { $set: { applicationsAmount: user["applicationsAmount"] + 1 }},
        function (error, result) {
            if (error) console.log(error);
        }
    );

    return App.create({
        appName: appName,
        developerID: id,
        description: description,
        imgSrc: imgSrc,
        downloadLink: downloadLink
    }, function (error, result) {
        if (error) throw error;
    });
};

exports.deleteApp = async function (appName) {
    let app = await getApp({ appName: appName});
    let user = await getUser({ _id: app["developerID"] });

    await User.updateOne(
        { _id: user["_id"] },
        { $set: { applicationsAmount: user["applicationsAmount"] - 1 }},
        function (error, result) {
            if (error) console.log(error);
        }
    );

    if (app["likes"] >= 500) {
        return false;
    } else {
        await App.deleteOne({ appName: appName }, function (error, result) {
            if (error) console.log(error);
        });
        return true;
    }
};

async function getApp (parameters) {
    let res = undefined;

    await App.findOne(parameters, function (err, result) {
        if (err) throw err;
        res = result;
    });

    return res;
}

exports.getAppInfo = async function (appName) {
    return await getApp({ appName: appName });
};

exports.getAppsNames = async function (login) {
    let user = await getUser({ login: login });

    return App.find({developerID: user["_id"]}, function (error, result) {
        if (error) console.log(error);
    })
};

exports.insertAppImage = async function (appName, imgSrc) {
    return await App.updateOne(
        { appName: appName },
        { $set: { imgSrc: imgSrc } }
    );
};

exports.uploadAppFile = async function (appName, link) {
    return await App.updateOne(
        { appName: appName },
        { $set: { downloadLink: link } }
    );
};

exports.updateAppInfo = async function (oldName, newName, description, downloadLink) {
    return await App.updateOne(
        { appName: oldName },
        { $set:
                {
                    appName: newName,
                    description: description,
                    downloadLink: downloadLink
                } },
        function (err, result) {
            if (err) return console.log(err);
        }
    );
};

exports.addDownloads = async function (appId) {
    let app = await App.findOne({ _id: appId });
    let downloads = app["downloads"]  + 1;
    return await App.updateOne({ _id: appId }, { $set: { downloads: downloads }})
};

exports.getTopUsersList = async function () {
    return User.find({ login: { $ne: "Admin"}, applicationsAmount: { $gt: 0} }).sort({ likes: "desc" });
};

exports.getTopAppsList = async function () {
    return App.find({}).sort({ likes: "desc" });
};

exports.like = async function (appId, login) {
    let user = await getUser({ login: login });
    let userId = user["_id"];
    let userLikes = user["likes"];
    let app = await getApp({ _id: appId });
    let appLikes = app["likes"];
    let isLiked = true;

    if (appLikes.includes(userId)) {
        isLiked = false;
        appLikes.splice(appLikes.indexOf(userId), 1);
        userLikes--;
    } else {
        appLikes.push(userId);
        userLikes++;
    }

    await User.updateOne(
        { _id: userId },
        { $set: { likes: userLikes }});

    await App.updateOne(
        { _id: appId },
        { $set: { likes: appLikes }});

    return { isLiked: isLiked, likeAmount: appLikes.length, userLikes: userLikes, nickname: user["nickname"] };
};

exports.getLikedApps = async function (login) {
    let user = await getUser({ login: login });
    let userId = user["_id"];
    let names = [];

    let apps = await App.find({});
    for (let app of apps) {
        if (app["likes"].includes(userId)) {
            names.push(app["appName"]);
        }
    }

    return names;
};

exports.getUserApps = async function (nickname) {
    let user = await getUser({ nickname: nickname });
    return App.find({ developerID: user["_id"] }).sort({ likes: "desc" });
};

exports.getAppsByName = async function (name) {
    return App.find({ appName: { $regex: "^" + name + ".*", $options: "i" } }).sort({ likes: "desc" });
};

exports.getUsersData = async function () {
    return User.find({ login: { $ne: "Admin" } }).sort( { nickname: "asc" } );
};

exports.getAppsData = async function () {
    return App.find({}).sort( { appName: "asc" } );
};

exports.updateUserData = async function (data) {
    return User.updateOne(
        { _id: data["_id"] },
        { $set:
                {
                    fullName: data["fullName"], nickname: data["nickname"], email: data["email"], phone: data["phone"],
                    bio: data["bio"], likes: data["likes"], applicationsAmount: data["applicationsAmount"],
                    imgSrc: data["imgSrc"]
                }
        });
};

exports.updateAppData = async function (data) {
    return App.updateOne(
        { _id: data["_id"] },
        { $set:
                {
                    appName: data["appName"], description: data["description"], likes: data["likes"],
                    downloads: data["downloads"], imgSrc: data["imgSrc"], downloadLink: data["downloadLink"]
                }
        });
};

exports.deleteUserData = async function (id) {
    return User.deleteOne({ _id: id });
};

exports.deleteAppData = async function (id) {
    return App.deleteOne({ _id: id });
};
