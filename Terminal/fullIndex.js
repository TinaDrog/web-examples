var socket = new WebSocket("ws://office.interpay.com.ua:585/");
console.log('here');
socket.onopen = function () {
    console.log("Соединение установлено.");
    socket.send('<?xml version="1.0" encoding="utf-8"?><request lang="ua"><getterminfo/></request>');
    socket.send('<?xml version="1.0" encoding="utf-8"?><request lang="ua"><menu_xml terminal_id="140103"/></request>');
    socket.send('<?xml version="1.0" encoding="utf-8"?><request lang="ua"><scenario_xml id="18" terminal_id="140103"/></request>');
};

socket.onclose = function (event) {
    if (event.wasClean) {
        console.log('Соединение закрыто чисто');
    } else {
        console.log('Обрыв соединения');
    }
    console.log('Код: ' + event.code + ' причина: ' + event.reason);
};

socket.onmessage = function (event) {
    console.log("Пришли данные");
    console.log("Получены данные " + event.data);
    //dataProcessing(event.data);
};

socket.onerror = function (error) {
    console.log("Ошибка " + error.message);
};

var structure, currentStructure;

class Question {
    constructor(fullText, briefText, id, answers) {
        this.fullText = fullText;
        this.briefText = briefText;
        this.id = id;
        this.answers = answers;
    }
}

class Answer {
    constructor(text, brief, id, equeryJoinId, question, results) {
        this.text = text;
        this.brief = brief;
        this.id = id;
        this.equeryJoinId = equeryJoinId;
        this.question = question;
        this.results = results;
    }

}

class Results {
    constructor(serviceName, serviceID, userParams) {
        this.serviceName = serviceName;
        this.serviceID = serviceID;
        this.userParams = userParams;
    }
}

class UserParams {
    constructor(name, type, mandatory, listPlaceholder, continuos, placeholder, prompt1, prompt2, inputHint, correctExample, mainPrompt, inputMask, regExpr, fixedValue) {
        this.name = name;
        this.type = type;
        this.mandatory = mandatory;
        this.listPlaceholder = listPlaceholder;
        this.continuos = continuos;
        this.placeholder = placeholder;
        this.prompt1 = prompt1;
        this.prompt2 = prompt2;
        this.inputHint = inputHint;
        this.correctExample = correctExample;
        this.mainPrompt = mainPrompt;
        this.inputMask = inputMask;
        this.regExpr = regExpr;
        this.fixedValue = fixedValue;
    }
}

function dataProcessing(data) {
    let parser = new DOMParser();
    let xmlDoc = parser.parseFromString(data,"text/xml");

    console.log('dataProcessing');
    structure = getQuestion(xmlDoc, 'response > question');
    //currentStructure = structure[0];
    console.log(structure);
}

function getQuestion(data, query) {
    let questionTags = data.querySelectorAll(query);
    if (questionTags.length === 0) {
        return null;
    }

    let rootQuestions = [];

    for (let questionTag of questionTags) {
        let answers = getAnswers(questionTag.children[3], query);
        let question = new Question(questionTag.children[0].innerHTML, questionTag.children[1].innerHTML,
            questionTag.children[2].innerHTML, answers);

        rootQuestions.push(question);
    }

    return rootQuestions;
}

function getAnswers(data, query) {
    query += ' > answers > answer';
    let answerTags = data.querySelectorAll(query);
    let result = [];

    for (let answerTag of answerTags) {
        let results = getResults(answerTag, query + ' > results');
        let question = getQuestion(answerTag, query + ' > question');
        let answer = new Answer(answerTag.getAttribute('text'), answerTag.getAttribute('brief'),
            answerTag.getAttribute('id'), answerTag.getAttribute('equery_join_id'), question, results);

        result.push(answer);
    }

    return result;
}

function getResults(data, query) {
    let resultsTag = data.querySelectorAll(query);
    if (resultsTag.length === 0) {
        return null;
    }

    let resultsData = resultsTag[0].children;
    let userParams = getUserParams(resultsData[2], query);

    return new Results(resultsData[0].innerHTML, resultsData[1].innerHTML, userParams);
}

function getUserParams(data) {
    let userParamsTags = data.querySelectorAll('userParam');
    if (userParamsTags.length === 0) {
        return null;
    }

    let result = [];

    for (let userParamsTag of userParamsTags) {
        let userParams = new UserParams(userParamsTag.getAttribute('name'), userParamsTag.getAttribute('type'),
            userParamsTag.getAttribute('mandatory'), userParamsTag.getAttribute('listPlaceholder'),
            userParamsTag.getAttribute('continuos'), userParamsTag.getAttribute('placeholder'),
            userParamsTag.getAttribute('prompt1'), userParamsTag.getAttribute('prompt2'),
            userParamsTag.getAttribute('inputHint'), userParamsTag.getAttribute('correctExample'),
            userParamsTag.getAttribute('mainPrompt'), userParamsTag.getAttribute('inputMask'),
            userParamsTag.getAttribute('regExpr'), userParamsTag.getAttribute('fixedValue'));
        result.push(userParams);
    }

    return result;
}