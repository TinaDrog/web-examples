function load(url, element)
{
    let req = new XMLHttpRequest();
    req.open("GET", url, false);
    req.send(null);

    element.innerHTML = req.responseText;
}

load("../testTemplates/header.html", document.getElementById("header"));
load("../testTemplates/footer.html", document.getElementById("footer"));