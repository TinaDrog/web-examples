var horizAmount = document.getElementById("horizAmount");
var vertAmount = document.getElementById("vertAmount");
var bombAmount = document.getElementById("bombAmount");

var startButton = document.getElementsByClassName("start-button")[0];
var menu = document.getElementsByClassName("menu")[0];
var mainText = document.getElementsByClassName("text__main")[0];
var additionalText = document.getElementsByClassName("text__additional")[0];

var container = document.createElement("div");
container.classList.add("container");

var fields = [];
var needToCheck = [];
var proper = [];

var isTapped = false;

startButton.addEventListener("click", function () {
    if (bombAmount.value > horizAmount.value * vertAmount.value - 1) {
        mainText.innerHTML = "A lot of bombs!";
        mainText.classList.add("text__main_animated");

        additionalText.innerHTML = "I know you wanna die, but you pointed";
        additionalText.classList.add("text__additional_animated");

        menu.classList.add("menu_dark");

        setTimeout(() => {
            mainText.innerHTML = "";
            mainText.classList.remove("text__main_animated");
            additionalText.innerHTML = "";
            additionalText.classList.remove("text__additional_animated");
            menu.classList.remove("menu_dark");
        }, 3000);

        return 0;
    }

    let width = Number(document.documentElement.clientWidth);
    let height = Number(document.documentElement.clientHeight);
    let fieldSize = width > height
        ? ((height - height * 0.2) / horizAmount.value)
        : ((width - width * 0.2) / vertAmount.value);

    container.style.width = horizAmount.value * fieldSize + horizAmount.value * 6 + "px";
    container.style.height = vertAmount.value * fieldSize + vertAmount.value * 6.2 + "px";

    for (let i = 0; i < vertAmount.value; i++) {
        fields.push([]);
        for (let j = 0; j < horizAmount.value; j++) {
            let field = document.createElement("div");
            field.setAttribute("id", `${i},${j}`);
            field.classList.add("field");
            field.style.width = fieldSize + "px";
            field.style.height = fieldSize + "px";
            field.setAttribute("data-nearBombAmount", "0");

            fields[i].push(field);
            container.appendChild(field);
        }
    }

    menu.remove();

    document.body.appendChild(container);
});

function countNumber(i, j) {
    for (let k = -1; k <= 1; k++) {
        for (let c = -1; c <= 1; c++) {
            try {
                fields[i+k][j+c].setAttribute("data-nearBombAmount",
                    parseInt(parseInt(fields[i+k][j+c].getAttribute("data-nearBombAmount")) + 1));
            }
            catch { }
        }
    }
}

function setNumber() {
    for (let i = 0; i < vertAmount.value; i++) {
        for (let j = 0; j < horizAmount.value; j++) {
            let t = parseInt(fields[i][j].getAttribute("data-nearBombAmount"));

            if (t !== 0 && !fields[i][j].classList.contains("bomb")) {
                fields[i][j].style.background = `navajowhite no-repeat center url(\"./images/${t}.png\")`;
                fields[i][j].style.backgroundSize = "0";
            } else if (!fields[i][j].classList.contains("bomb")) {
                fields[i][j].classList.add("empty");
            }
        }
    }
}

function generateElements() {
    // generate bombs
    for (let c = 0; c < bombAmount.value; c++) {
        let i = Math.floor(Math.random() * vertAmount.value);
        let j = Math.floor(Math.random() * horizAmount.value);
        fields[i][j].classList.contains("bomb") || fields[i][j].classList.contains("empty")
            ? c--
            : fields[i][j].classList.add("bomb");
    }

    // generate numbers near bombs
    for (let i = 0; i < vertAmount.value; i++) {
        for (let j = 0; j < horizAmount.value; j++) {
            if (fields[i][j].classList.contains("bomb")) {
                countNumber(i, j);
            }
        }
    }
    setNumber();
}

function gameOver() {
    mainText.innerHTML = "You lose!";
    additionalText.innerHTML = "you were killed by COVID-19";

    container.classList.add("container_dark");
}

function findCurrentField(event) {
    let tappedFieldId = event.target.getAttribute("id");
    for (let i = 0; i < vertAmount.value; i++) {
        for (let j = 0; j < horizAmount.value; j++) {
            if (fields[i][j].getAttribute("id") === tappedFieldId) {
                return fields[i][j];
            }
        }
    }
}

function check(event, i, j) {
    for (let k = i - 1; k <= i + 1; k++) {
        for (let c = j - 1; c <= j + 1; c++) {
            if (k === i && c === j) {
                continue;
            }

            try {
                if (fields[k][c].classList.contains("bomb")) {
                    return;
                }
                if (fields[k][c].getAttribute("data-isOpened") !== "opened") {
                    if (fields[k][c].getAttribute("data-nearBombAmount") === "0") {
                        needToCheck.push([]);
                        needToCheck[needToCheck.length - 1].push(k);
                        needToCheck[needToCheck.length - 1].push(c);
                    }
                    proper.push([]);
                    proper[proper.length - 1].push(k);
                    proper[proper.length - 1].push(c);
                }
            }
            catch (e) { }
        }
    }

    if (proper.length > 0) {
        openField(event);
    }

    while (needToCheck.length > 0) {
        let i = needToCheck[needToCheck.length - 1][0], j = needToCheck[needToCheck.length - 1][1];
        if (i === undefined || j === undefined) {
            needToCheck.pop();
            continue;
        }

        fields[i][j].setAttribute("data-isOpened", "opened");
        let index = needToCheck.pop();
        check(event, index[0], index[1]);
    }
}

function openField(event) {
    for (let i = 0; i < proper.length; i++) {
        if (fields[proper[i][0]][proper[i][1]].getAttribute("data-isOpened") !== "opened") {
            fieldClick(event, fields[proper[i][0]][proper[i][1]], false);
        }
    }
}

function win() {
    let flags = 0, opened = 0;
    for (let i = 0; i < vertAmount.value; i++) {
        for (let j = 0; j < horizAmount.value; j++) {
            if (fields[i][j].getAttribute("data-isOpened") === "opened") {
                opened++;
            }
            if (fields[i][j].classList.contains("flag")) {
                flags++;
            }
        }
    }

    if (flags + opened === horizAmount.value * vertAmount.value && flags === parseInt(bombAmount.value)) {
        mainText.innerHTML = "You win";
        additionalText.innerHTML = "but COVID-19 still waiting for you";

        container.classList.add("container_dark");
    }
}

function fieldClick(event, field = event.target, isCheckNeeded = true) {
    if (field !== container) {
        let currentField = field === event.target
            ? findCurrentField(event)
            : field;

        if (isTapped && !currentField.classList.contains("flag")) {
            currentField.setAttribute("data-isOpened", "opened");

            if (currentField.classList.contains("bomb")) {
                for (let i = 0; i < vertAmount.value; i++) {
                    for (let j = 0; j < horizAmount.value; j++) {
                        fields[i][j].style.backgroundColor = "white";
                        fields[i][j].style.backgroundSize = "75%";
                    }
                }
                gameOver();
            } else if (currentField.getAttribute("data-nearBombAmount") > 0){
                currentField.style.backgroundColor = "white";
                currentField.style.backgroundSize = "80%";
            } else {
                currentField.style.background = "white";
                if (isCheckNeeded) {
                    let index = field.getAttribute("id").split(',');
                    let i = parseInt(index[0]), j = parseInt(index[1]);

                    check(event, i, j);
                }
            }
        } else if (!isTapped){
            isTapped = true;
            currentField.classList.add("empty");
            currentField.setAttribute("data-isOpened", "opened");
            generateElements();
            fieldClick(event);
        }
        win();
    }
}

container.addEventListener("click", fieldClick);

container.addEventListener("contextmenu", function (event) {
    event.preventDefault();
    if (event.target !== container) {
        let currentField = findCurrentField(event);
        if (currentField.getAttribute("data-isOpened") !== "opened") {
            currentField.classList.toggle("flag");
        }
        win();
    }
});
